import React, { useEffect, useState, useContext } from 'react';
import { useHistory, useParams } from 'react-router-dom'
import { Select, message, Button, Modal, InputNumber, Collapse, Radio, DatePicker, Checkbox } from 'antd'
import imgdetail1 from '../../images/detail1.jpg'
import ModalPopup from '../../components/Modal'
import { addCartToLocal } from '../../plugins/localstorage'

import { EnvironmentOutlined, EditOutlined } from "@ant-design/icons";
import ApiRestaurant from '../../services/restaurant-service'
// import ApiUser from '../../services/user-service'
import Autocomplete from "../../components/Autocomplete";
import UserContext from '../../context/info-context'
import queryString from "query-string"
import moment from 'moment';
import "./AutoQuote.scss"

const { Panel } = Collapse;
const listEvent = [
    { value: 'tieccuoi', label: 'Tiệc cưới'},
    { value: 'hoinghi', label: 'Hội nghị/hội thảo'},
    { value: 'lienhoan', label: 'Liên hoan/Tiệc'},
    { value: 'sinhnhat', label: 'Sinh nhật'},
    { label: 'Họp nhóm', value: "hopnhom" },
    { label: 'Workshop', value: "workshop" },
]
const propertiesMapping = {
    "bantron": "Bàn tròn",
    "ngoaitroi": "Ngoài trời",
    "sankhau": "Sân khấu",
    "bancong": "Ban công",
    "tiecdung": "Tiệc đứng"
}
const AutoQuote = () => {
    const history = useHistory()
    const param = useParams()
    const {quoteValue, setQuoteValue} = useContext(UserContext)
    console.log(quoteValue)
    const [listRestaurant, setListRestaurant] = useState([])
    const [listResRecommend, setListResRecommend] = useState([])
    const [listResId, setListResId] = useState([])
    const [numberPeople, setNumberPeople] = useState(quoteValue.quantity || 0)
    const [totalPrice, setTotalPrice] = useState(quoteValue.price || 0)
    const [tabQuote, setTabQuote] = useState(quoteValue.priority || 'price');
    const [event, setEvent] = useState(quoteValue.type || null)
    const [addrDetail, setAddrDetail] = useState('')
    const [coordinate, setCoordinates] = useState(quoteValue.coordinate || [])
    const [utilities, setUtilities] = useState([]);
    const [services, setServices] = useState([]);
    const [selectedUtilities, setSelectedUtilities] = useState(quoteValue.services || []);
    const [selectedServices, setSelectedServices] = useState(quoteValue.properties || []);
    
    const onChangeTab = (e) => {
        setTabQuote(e)
    }
    const onChangePriority = (v) => {
        setTabQuote(v)
    }
    const onChangeEvent = (e) => {
        setEvent(e)
    }
    const onChangePeople = (e) => {
        setNumberPeople(e)
    }
    const onChangePrice = (e) => {
        setTotalPrice(e)
    }
    const handleChangeUtilites = (value) => {
        setSelectedUtilities(value);
    }
    const handleChangeServices = (value) => {
        setSelectedServices(value);
    }
    const initListRestaurant = async (formData) => {
        const response = await ApiRestaurant.getRestaurantInquery(formData)
        if(response.ok) {
          setListRestaurant(response.data)
          setListResId(response.data.map(e => e._id))
        }
    }
    const initListResRecommend = async (formData) => {
        const response = await ApiRestaurant.getRestaurantInquery({ type: formData.type, quantity: formData.quantity, isRecommend: true})
        if(response.ok) {
            setListResRecommend(response.data)
        }
    }

    const MenuDetail = (resInfo) => {
        const menu = resInfo.menu && resInfo.menu.length > 0 && resInfo.menu[0]
        const [selectedMenu, setSelectMenu] = useState(menu)
        const [selectedDate, setSelectDate] = useState(null)
        const onChangeMenu = (mn) => {
            setSelectMenu(mn)
        }
        const getPrice = () => {
            const lstService = services.filter(s => selectedServices.includes(s.type))
            let price = 0;
            lstService.forEach(s => price += s.price)
            return price + selectedMenu.price * selectedMenu.totalTable
        }
        const disabledDate = (current) => {
            return (current && current < moment().endOf('day')) || resInfo.reserved.includes(current.format("DD MM YYYY"));
        }
        const onChangeDate = (v) => {
            setSelectDate(v.unix())
        }
        return <div className="modal-menu-detail">
            <h3>Chi tiết đơn hàng</h3>
            <div className="list-item">
                <div className="item item1">
                    <div className="block-1">
                        <div className="block name">
                            <span>Tên sản phẩm</span>
                        </div>
                        <div className="block unit-price">
                            <span>Số người</span>
                        </div>
                        <div className="block unit-price">
                            <span>Đơn giá</span>
                        </div>
                        <div className="block quantity">
                            <span>Số lượng</span>
                        </div>
                        <div className="checkbox">
                        </div>
                    </div>
                </div>
                {services.filter(s => selectedServices.includes(s.type)).map(s => <div className="item item2">
						<div className="block-1">
							<div className="block name">
								<span>{s.name}</span>
							</div>
                            <div className="block unit-price">
                                <span>{""}</span>
                            </div>
							<div className="block unit-price">
								<span>{s.price.toLocaleString("it-IT")}</span>
							</div>
							<div className="block quantity">
								<span>{1}</span>
							</div>
                            <div className="checkbox">
                            </div>
						</div>
                </div>)}
                {resInfo.menu.map((mn) => {
                
                    return <div className="item item2">
                        <div className="block-1">
                            <div className="block name">
                                <span>{mn.name}</span>
                            </div>
                            <div className="block unit-price">
                                <span>{mn.numberPeople}</span>
                            </div>
                            <div className="block unit-price">
                                <span>{mn.price.toLocaleString("it-IT")}</span>
                            </div>
                            <div className="block quantity">
                                <span>{mn.totalTable}</span>
                            </div>
                            <div className="checkbox">
                                <Radio onChange={() => onChangeMenu(mn)} checked={selectedMenu._id === mn._id}></Radio>
                            </div>
                        </div>
                        {selectedMenu._id === mn._id && <div className="detail">
                            <Collapse defaultActiveKey={['1']}>
                                <Panel header="Menu chi tiết" key="1">
                                    {mn.foodList.map(e => {
                                        return <div>{e}</div>
                                    })}
                                </Panel>
                            </Collapse>
                        </div>}
                    </div>})}
                
            </div>
            <div className="total-price">
                <div className="text">
                    <span>Chi phí dự tính</span>
                </div>
                <div className="price">
                    <span>{getPrice().toLocaleString("it-IT", {style : "currency", currency : "VND"})}</span>
                </div>
            </div>
            <div className="total-price">
                <div className="text">
                    <span>Ngày tổ chức</span>
                </div>
                <div className="price">
                    <DatePicker 
                        disabledDate={(current) => disabledDate(current)} 
                        onChange={onChangeDate}
                    />
                </div>
            </div>
            <div className="group-btn" style={{ marginTop: '20px', display: 'flex', justifyContent: 'flex-end'}}>
                <Button onClick={() => onClickAddCart(resInfo, menu, selectedDate)}>Thêm vào giỏ hàng</Button>
                <Button type="primary" style={{ marginLeft: '10px'}} onClick={() => onClickDeposit(resInfo, menu, selectedDate)}>Đặt cọc ngay</Button>
                
            </div>
        </div>
    }
    const toggleModalRestaurant = (resInfo) => {
        return  ModalPopup({
            okText: "Tìm kiếm",
            content: () => MenuDetail(resInfo),
            className: "ant-modal-menu-detail"
        })
    }


    const onClickAddCart = (resInfo, menu, selectedDate) => {
        if(!selectedDate) return message.error("Bạn cần chọn ngày tổ chức.")
        addCartToLocal(
            {
                restaurant_name: resInfo.restaurant.name,
                restaurant_id: resInfo.restaurant._id,
                venueId: menu.venueId[0],
                item_type: "menu",
                selectedDate,
                item: {
                    item_name: menu.name,
                    item_price: menu.price,
                    item_quantity: menu.totalTable,
                    item_image: menu.image,
                    item_id: menu._id,
                    item_type: "menu",
                }
            }
        )
        let lstService = services.filter(s => selectedServices.includes(s.type))
        for(let sv of lstService) {
            addCartToLocal(
                {
                    restaurant_name: resInfo.restaurant.name,
                    restaurant_id: resInfo.restaurant._id,
                    item_type: "service",
                    selectedDate,
                    item: {
                        item_name: sv.name,
                        item_price: sv.price,
                        item_quantity: 1,
                        item_image: sv.images[0],
                        item_id: sv._id,
                        item_type: "service",
                    }
                }
            )
        }
        Modal.destroyAll()
    }
    const onClickDeposit = (resInfo, menu, selectedDate) => {
        if(!selectedDate) return message.error("Bạn cần chọn ngày tổ chức.")
        console.log(selectedDate)
        addCartToLocal(
            {
                restaurant_name: resInfo.restaurant.name,
                restaurant_id: resInfo.restaurant._id,
                venueId: menu.venueId[0],
                item_type: "menu",
                selectedDate,
                item: {
                    item_name: menu.name,
                    item_price: menu.price,
                    item_quantity: menu.totalTable,
                    item_image: menu.image,
                    item_id: menu._id,
                    item_type: "menu",
                }
            }
        )
        let lstService = services.filter(s => selectedServices.includes(s.type))
        for(let sv of lstService) {
            addCartToLocal(
                {
                    restaurant_name: resInfo.restaurant.name,
                    restaurant_id: resInfo.restaurant._id,
                    item_type: "service",
                    selectedDate,
                    item: {
                        item_name: sv.name,
                        item_price: sv.price,
                        item_quantity: 1,
                        item_image: sv.images[0],
                        item_id: sv._id,
                        item_type: "service",
                    }
                }
            )
        }
        history.push(`/cart?rest_id=${resInfo.restaurant._id}`)
        Modal.destroyAll()
    }

    const onClickQuote = async () => {
        if(!event) return message.error("Vui lòng chọn loại hình")
        if(!numberPeople) return message.error("Vui lòng nhập số người")
        // if(tabQuote === 'price' && !totalPrice) return message.error("Vui lòng nhập chi phí")
        // if(tabQuote !== 'price' && !coordinate) return message.error("Vui lòng nhập địa chỉ")
        let dataQuote = { 
            type: event, quantity: numberPeople, 
            price: totalPrice, coordinate, 
            properties: selectedUtilities, services: selectedServices ,
            priority: tabQuote
        }
        initListRestaurant(dataQuote)
        initListResRecommend(dataQuote)

    }
    const initListService = async () => {
        const res = await ApiRestaurant.getService()
        if(res.ok) {
            setServices(res.data)
        }
    }
    const initListProperties = async () => {
        const res = await ApiRestaurant.getProperties()
        if(res.ok) {
            setUtilities(res.data)
        }
    }
    const gotoDetail = (e, id) => {
        e.stopPropagation();
        history.push(`/page-detail/${id}`)
    }
    useEffect(() => {
        const q = queryString.parse(history.location.search)
        if(q.event) {
            let formData = {
                type: q.event
            }
            setEvent(q.event)
            if(q.quantity) {
                formData.quantity = parseInt(q.quantity)
                setNumberPeople(parseInt(q.quantity))
            }
            if(q.price) {
                formData.price = q.price
                setTotalPrice(parseInt(q.price))
            }
            console.log(q)
            initListRestaurant(formData)
            initListResRecommend(formData)
        } else {
            initListRestaurant(quoteValue)
            initListResRecommend(quoteValue)
        }
        initListService()
        initListProperties()
    }, [])
    return (
        <div className="wrap-autoquote">
            <div className="autoquote-search">
                <div className="auto-quote">
                    <div className="block">
                        <h3>Báo giá tự động</h3>
                    </div>
                    {/* <div className="block-1">
                        <div className={`block-1__item${tabQuote === 'price' ? '-active' : ''}`} onClick={() => onChangeTab('price')}>
                            Theo chi phí
                        </div>
                        <div className={`block-1__item${tabQuote === 'address' ? '-active' : ''}`} onClick={() => onChangeTab('address')}>
                            Theo vị trí
                        </div>
                    </div> */}
                    <div className="block-2">
                        <div className="block-2__item totalPrice">
                            <h3>* Loại hình</h3>
                            <Select style={{ width: '100%' }} value={event} onChange={onChangeEvent}>
                                {listEvent.map(event => (
                                    <Select.Option value={event.value} key={event.value}>{event.label}</Select.Option>
                                ))}
                            </Select>
                        </div>
                        <div className="block-2__item capacity">
                            <h3>Số người</h3>
                            <InputNumber value={numberPeople} onChange={onChangePeople}></InputNumber>
                        </div>
                    </div>
                    <div className="block-4 block-45">
                        <div className="block-4__item price block-45__item">
                            <div className="item-top">
                                <h3>Chi phí dự tính</h3>
                                {/* <div className="right">
                                    Ưu tiên <Checkbox onChange={() => onChangePriority("price")} checked={tabQuote === "price"}></Checkbox>
                                </div> */}
                            </div>
                            <div style={{ display: 'flex', alignItems: 'center'}}>
                            <InputNumber
                                value={totalPrice}
                                onChange={onChangePrice}
                                style={{width: "100%"}}
                                formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                                parser={value => value.replace(/\$\s?|(,*)/g, '')}
                            ></InputNumber>VND
                            </div>
                        </div>
                    </div>
                    <div className="block-5 block-45">
                        <div className="block-5__item address block-45__item">
                            <div className="item-top">
                                <h3>Địa chỉ gần nhất</h3>
                                {/* <div className="right">
                                    Ưu tiên <Checkbox onChange={() => onChangePriority("address")} checked={tabQuote === "address"}></Checkbox>
                                </div> */}
                            </div>
                            <Autocomplete 
                                setAddrDetail = {setAddrDetail}
                                setCoordinates = {setCoordinates}
                                addressText={quoteValue.addressText}
                                name="address_detail"
                            />
                        </div>
                    </div>
                    <div className="block-3">
                        <div className="block-3__item utilities">
                            <h3>Tiện ích</h3>
                            <Select value={selectedUtilities} mode="tags" style={{ width: '100%' }} onChange={handleChangeUtilites} tokenSeparators={[',']}>
                                {utilities.map((e, idx) => {
                                    return <Option value={e} key={idx}>{propertiesMapping[e]}</Option>
                                })}
                            </Select>
                        </div>
                    </div>
                    <div className="block-3">
                        <div className="block-3__item services">
                            <h3>Dịch vụ</h3>
                            <Select  value={selectedServices} mode="tags" style={{ width: '100%' }} onChange={handleChangeServices} tokenSeparators={[',']}>
                                {services.map((e, idx) => {
                                    return <Option value={e.type} key={idx}>{e.name}</Option>
                                })}
                            </Select>
                        </div>
                    </div>
                    <div className="btn-group" style={{ marginTop: '20px', display: 'flex', justifyContent: 'flex-end'}}>
                        <Button type="primary" style={{ marginLeft: '10px'}} onClick={onClickQuote}>Tìm kiếm</Button>
                    </div>
                </div>
            </div>
            <div className="container">
            <div className="container-autoquote">
                <div className="container-autoquote-left">
                    <h3>Danh sách nhà hàng phù hợp</h3>
                    <div className="list-restaurant">
                        {listRestaurant.length?listRestaurant.map(e => {
                            let resInfo = e.restaurant
                            let menu = e.menu[0]
                            return <div className="list-restaurant-item" onClick={() => toggleModalRestaurant(e)}>
                                <div className="list-restaurant-item__img">
                                    <img src={resInfo.listImage[0] || imgdetail1} alt=""/>
                                </div>
                                <div className="list-restaurant-item__info">
                                    <h3>{resInfo.name}</h3>
                                    <p className="address">
                                        <EnvironmentOutlined  style={{ color: "#002f79"}} /> 
                                        {`${resInfo.address}`}</p>
                                    <div className="menu-detail">
                                        <div className="block menu-name"><span className="key">Menu:</span> <span className="value">{menu.name}</span></div>
                                        <div className="block menu-numberPeople"><span className="key">Số người/bàn:</span> <span className="value">{menu.numberPeople}</span></div>
                                        <div className="block menu-totalTable"><span className="key">Số bàn:</span> <span className="value">{menu.totalTable}</span></div>
                                        <div className="block menu-price"><span className="key">Đơn giá:</span><span className="value">{menu.price.toLocaleString("it-IT", {style : "currency", currency : "VND"})}</span></div>
                                    </div>
                                    <div className="price">
                                        <p style={{ color: "#002f79"}}>Tổng chi phí dự tính</p>
                                        <p style={{ color: "#fc7702"}}>{menu.totalPrice.toLocaleString("it-IT", {style : "currency", currency : "VND"})}</p>
                                    </div>
                                    {e.distance && coordinate.length > 0 && <div className="distance">
                                    <EnvironmentOutlined />{e.distance && e.distance.toFixed(1)} km
                                    </div>}
                                    <div className="detail">
                                        <span onClick={(e) => gotoDetail(e, resInfo._id)}>Go to detail</span>
                                    </div>
                                </div>
                            </div>}): <div>
                                <p>Không tìm thấy nhà hàng phù hợp với yêu cầu</p>
                            </div>}
                    </div>
                </div>
            </div>
            <div className="container-autoquote">
                <div className="container-autoquote-left">
                    <h3>Nhà hàng gợi ý</h3>
                    <div className="list-restaurant">
                        {listResRecommend.length?listResRecommend.filter(r => !listResId.includes(r._id)).map(e => {
                            let resInfo = e.restaurant
                            let menu = e.menu[0]
                            if(!menu) return;
                            return <div className="list-restaurant-item" onClick={() => toggleModalRestaurant(e)}>
                                <div className="list-restaurant-item__img">
                                    <img src={resInfo.listImage[0] || imgdetail1} alt=""/>
                                </div>
                                <div className="list-restaurant-item__info">
                                    <h3>{resInfo.name}</h3>
                                    <p className="address">
                                        <EnvironmentOutlined  style={{ color: "#002f79"}}/> 
                                        {`${resInfo.address}`}</p>
                                    <div className="menu-detail">
                                        <div className="block menu-name"><span className="key">Menu:</span> <span className="value">{menu.name}</span></div>
                                        <div className="block menu-numberPeople"><span className="key">Số người/bàn:</span> <span className="value">{menu.numberPeople}</span></div>
                                        <div className="block menu-totalTable"><span className="key">Số bàn:</span> <span className="value">{menu.totalTable}</span></div>
                                        <div className="block menu-price"><span className="key">Đơn giá:</span><span className="value">{menu.price.toLocaleString("it-IT", {style : "currency", currency : "VND"})}</span></div>
                                    </div>
                                    <div className="price">
                                        <p style={{ color: "#002f79"}}>Tổng chi phí dự tính</p>
                                        <p style={{ color: "#fc7702"}}>{menu.totalPrice.toLocaleString("it-IT", {style : "currency", currency : "VND"})}</p>
                                    </div>
                                    {/* {tabQuote === "address" && coordinate.length > 0 && <div className="distance">
                                        {e.distance && e.distance.toFixed(1)} km
                                    </div>} */}
                                    <div className="detail">
                                        <span onClick={(e) => gotoDetail(e, resInfo._id)}>Go to detail</span>
                                    </div>
                                </div>
                            </div>}): <div>
                                <p>Không tìm thấy nhà hàng phù hợp với yêu cầu</p>
                            </div>}
                    </div>
                </div>
            </div></div>
        </div>
    );
}

export default AutoQuote;