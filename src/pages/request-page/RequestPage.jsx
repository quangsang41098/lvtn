import React, { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom'
import ApiRestaurant from '../../services/restaurant-service'
import ApiUser from '../../services/user-service'
import { CheckOutlined, CloseOutlined } from '@ant-design/icons'
import { Table } from 'antd'
import ModalPopup from '../../components/Modal'
import './RequestPage.scss'

const RequestPage = () => {
    const history = useHistory()
    const columns = [
        {
          title: 'Name',
          dataIndex: 'name',
        },
        {
          title: 'Email',
          dataIndex: 'email',
        },
        {
          title: 'Address',
          dataIndex: 'address',
        },
        {
          title: 'Action',
          dataIndex: 'action',
            render: (_, record) => {
                return <div className="action" style={{display:'flex'}}>
                    <div onClick={(e) => onClickAccept(e, record)}><CheckOutlined /></div>
                    {/* <div onClick={(e) => onClickDecline(e, record)}><CloseOutlined /></div> */}
                </div>
            }
        },
    ];
    const onClickAccept = (e, record) => {
        e.stopPropagation();
        ModalPopup({
            type: "confirm",
            content: () => <div>Cho phép nhà hàng hiển thị trên trang chủ?</div>,
            onOk: async () => {
                const res = await ApiUser.acceptRestaurant(record.key)
                if(res.ok) initRestaurantPending()
                else {
                    ModalPopup({
                        type: "error",
                    })

                }
            }
        })
    }
    const onClickDecline = (e, res) => {
        e.stopPropagation();
        
    }
    const [listRestaurantPending, setListRestaurantPending] = useState([])
    const initRestaurantPending = async () => {
        const response = await ApiRestaurant.getRestaurantPending()
        const res = response.data.map(e => {
            return {
                key: e._id,
                name: e.name,
                email: e.email,
                address: e.address,
            }       
        })
        setListRestaurantPending(res)
    }
    useEffect(() => {
        initRestaurantPending()
    },[])

    return (
        <div className="request-page">
            <h3>Danh sách yêu cầu</h3>
            <Table 
                columns={columns} 
                dataSource={listRestaurantPending} 
                rowClassName="request-page-row" 
                onRow={(record, rowIndex) => {
                    return {
                      onClick: event => { 
                          history.push(`/page-detail/${record.key}`)
                       },
                    };
                }}
            />
        </div>
    );
}
export default RequestPage