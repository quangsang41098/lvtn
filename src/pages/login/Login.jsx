import React, { useEffect, useState, useContext } from 'react';
import { useHistory } from 'react-router-dom'
import { Form, Input, Button, message, PageHeader } from 'antd'
import ApiUser from '../../services/user-service'
import "./Login.scss"
import UserContext from '../../context/info-context'
import ModalPopup from "../../components/Modal"
const Login = () => {
    const history = useHistory()
    const userContext = useContext(UserContext)
    const [isSignIn, setIsSignIn] = useState(true)
    const onFinish = async (data) => {
        if(!isSignIn) {
            const res = await ApiUser.registerUser(data)
            console.log(res)
            if(res.ok) {
                // message.success('Đăng kí thành công')
                // setIsSignIn(!isSignIn)
                ModalPopup({
                    type: "confirm",
                    title: "Xác thực email",
                    okText: "Verify",
                    content: () => <div>
                        <p>Chúng tôi đã gửi cho bạn một email.</p>
                        <p>Để hoàn thành đăng kí, xin vui lòng truy cập email để xác thực</p>
                        </div>,
                    className: "ant-modal-menu-detail",
                    onOk: () => window.open( 'http://www.gmail.com')
                })
            }   
            else {
                message.error("Email đã tồn tại!")
            }
        } else {
            const res = await ApiUser.loginUser(data)
            if(res.ok) {
                userContext.getUserInfo()
                message.success('Đăng nhập thành công')
                history.push("/home")
            } else {
                message.error('Tên đăng nhập hoặc mật khẩu không đúng!')
            }
        }
    }

    return (
        <React.Fragment>
        <PageHeader
            className="site-page-header"
            onBack={() => history.push("home")}
            title="Back to home"
        />
        <div className="wrap-login">
            <Form
                name="sign-in"
                onFinish={onFinish}
            >
                <h2>{isSignIn ? "Đăng nhập" :  "Đăng kí"}</h2>
                {!isSignIn && <>
                    <p>Họ và tên *</p>
                    <Form.Item
                        name="name"
                        rules={[{ required: true, message: 'Please input your name!' }]}
                    >
                        <Input placeholder="Name"/>
                    </Form.Item></>}
                <p>Email *</p>
                <Form.Item
                    name="email"
                    rules={[{ required: true, message: 'Please input your email!' }, { type: 'email' }]}
                >
                    <Input placeholder="Email"/>
                </Form.Item>

                <p>Password *</p>
                <Form.Item
                    name="password"
                    rules={[{ required: true, message: 'Please input your password!' }]}
                >
                    <Input.Password placeholder="Password"/>
                </Form.Item>

                <div className="block-btn">
                    <div className="block-left">
                        <span>{isSignIn && "Chưa có tài khoản? "}
                            <span className="sign-up" onClick={() => setIsSignIn(!isSignIn)}>
                                {isSignIn ? "Đăng kí ngay" : "Đăng nhập"}
                            </span></span>
                    </div>
                    <Button type="primary" htmlType="submit">
                        {isSignIn ? "Đăng nhập" :  "Đăng kí"}
                    </Button>

                </div>
                </Form>
        </div>
        </React.Fragment>
    );
}

export default Login;