import React, { useState, useContext, useEffect } from "react";
import { Link, useHistory } from "react-router-dom";
import {
	CardNumberElement,
	CardExpiryElement,
	CardCVCElement,
	injectStripe
} from "react-stripe-elements";
import axios from "axios";
import UserContext from '../../context/info-context'
import { PageHeader, Collapse, message, Button } from 'antd'
import ApiRestaurant from '../../services/restaurant-service'
import "./PaymentForm.scss";

const { Panel } = Collapse;
const PaymentForm = ({ selectedProduct, stripe }) => {
    const { selectedCart } = useContext(UserContext)
	// cart info here
	console.log(selectedCart)
	const history = useHistory()
	const [isLoading, setIsLoading] = useState(false);
	const [restInfo, setRestInfo] = useState({});
	const [listMenu, setListMenu] = useState([]);
	const [listService, setListService] = useState([]);
	if (selectedCart === null) {
		history.push("/cart");
		return null;
	}
	if (selectedProduct === null) {
		history.push("/");
		return null;
	}

	const handleSubmit = async event => {
		event.preventDefault();
		const billingEmail = document.getElementById("billingEmail").value;
		const cardName = document.getElementById("cardName").value;
		setIsLoading(true)
		const order = await axios.post(`${process.env.REACT_APP_API_URL || "http://localhost:4300"}/payment/checkout`, {
			amount: selectedCart.amount,
			// source: token.token.id,
			billingEmail: billingEmail,
			venueId: selectedCart.venue,
			menu: selectedCart.menu,
			time: selectedCart.time,
			services: selectedCart.services,
			userId: selectedCart.userId,
			totalPrice: selectedCart.total
		});
		setIsLoading(false)
		let receiptUrl = order.data
		// message.success("Payment Successful!")
		localStorage.removeItem('cart')
		// history.push("/users/orders")
		window.location.href = receiptUrl;
		// window.open(receiptUrl)
		// stripe.createToken({
		// 	name: cardName,
		// 	email: billingEmail
		// }).then(async token => {
		// 	if(token.error){
		// 		alert("payment fail");
		// 		return;
		// 	}
		// });
	};

    const initRestaurantDetail = async (id) => {
        const response = await ApiRestaurant.getDetailRestaurant(id)
        if(response.ok) {
            let res = response.data.restaurant
            setRestInfo(res)
			let venue = res.venueList.find(v => v._id === selectedCart.venue)
			let menus = []
			
			for(let i of selectedCart.menu) {
				let m = venue.menus.find(e => e._id === i.menuId)
				menus.push({ menuInfo: m, quantity: i.quantity})
			}
			setListMenu(menus)
        }
    }
    const initListService = async () => {
        const res = await ApiRestaurant.getService()
        if(res.ok) {
            setListService(res.data.filter(s => selectedCart.services.includes(s._id)))
        }
    }
	useEffect(() => {
		if(selectedCart.rest_id) initRestaurantDetail(selectedCart.rest_id)
		if(selectedCart.services && selectedCart.services.length > 0) initListService()
	}, [selectedCart] )
	useEffect(() => {
		let s = document.getElementById("billingEmail")
		s.value = selectedCart.receipt_email
	}, [])
	return (
		<React.Fragment>
        <PageHeader
            className="site-page-header"
            onBack={() => history.push("/cart")}
            title="Back to cart"
        />
		<div className="payment-container">
			<div className="checkout-form" style={{ display: 'none'}}>
				<p>Amount: {selectedCart.amount.toLocaleString("it-IT", {style : "currency", currency : "VND"})}</p>
				<form onSubmit={handleSubmit}>
					<label>
						Billing email
						<input id="billingEmail" type="text" placeholder="Enter email" />
					</label>
					<label>
						Cardholder Name
						<input id="cardName" type="text" placeholder="Enter full name" />
					</label>
					<label>
						Card number
						<CardNumberElement />
					</label>
					<label>
						Expiration date
						<CardExpiryElement />
					</label>
					<label>
						CVC
						<CardCVCElement />
					</label>
					<Button  type="primary" htmlType="submit" className="order-button" loading={isLoading}>
						Pay
					</Button>
				</form>
			</div>
			<div className="modal-menu-detail">
				<h3>Chi tiết đơn hàng</h3>
				<div className="list-item">
					<div className="item item1">
						<div className="block-1">
							<div className="block name">
								<span>Tên sản phẩm</span>
							</div>
							<div className="block unit-price">
								<span>Đơn giá</span>
							</div>
							<div className="block quantity">
								<span>Số lượng</span>
							</div>
						</div>
					</div>
					
					{listMenu.map(m => <div className="item item2">
						<div className="block-1">
							<div className="block name">
								<span>{m.menuInfo.name}</span>
							</div>
							<div className="block unit-price">
								<span>{m.menuInfo.price.toLocaleString("it-IT", {style : "currency", currency : "VND"})}</span>
							</div>
							<div className="block quantity">
								<span>{m.quantity}</span>
							</div>
						</div>
						<div className="detail">
							<Collapse defaultActiveKey={['1']}>
								<Panel header="Danh sách món ăn" key="1">
									{m.menuInfo.foodList.map(e => {
											return<div>{e}</div>
										}
									)}
								</Panel>
							</Collapse>
						</div>
					</div>)}
					{listService.map(s => <div className="item item2">
						<div className="block-1">
							<div className="block name">
								<span>{s.name}</span>
							</div>
							<div className="block unit-price">
								<span>{s.price.toLocaleString("it-IT", {style : "currency", currency : "VND"})}</span>
							</div>
							<div className="block quantity">
								<span>{1}</span>
							</div>
						</div>
					</div>)}
					
					<div className="price" style={{ display: 'flex', justifyContent: 'space-between', marginTop: '2em', color: '#002f79', width: '50%'}}>
						<p>Tổng chi phí</p>
						<p style={{color: "#e0a800 "}}>{selectedCart.total.toLocaleString("it-IT", {style : "currency", currency : "VND"})}</p>
					</div>
				</div>
				<Button  type="primary" onClick={handleSubmit} className="order-button" loading={isLoading}>
					Thanh toán
				</Button>
			</div>
		</div>
		
		</React.Fragment>
	);
};

export default injectStripe(PaymentForm);