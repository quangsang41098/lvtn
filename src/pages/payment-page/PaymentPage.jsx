import React from "react";
import { StripeProvider, Elements } from "react-stripe-elements";
import PaymentForm from "./PaymentForm";

// const PaymentPage = ({ history }) => {
// 	useEffect(() => {
// 		window.scrollTo(0, 0);
// 	}, []);
// 	const selectedProduct = localStorage.getItem("cart");
// 	return (
// 		<StripeProvider apiKey="pk_test_51IdHtcBE2zBABmfMjO1abGMinLBL367co4OwBcEZWycAhL4lRtJa4Iab6NSnWFkTlUz6OmrCfaAtWbt3IHLJdkXo00DX0IkCBt">
// 			<Elements>
// 				<PaymentForm selectedProduct={selectedProduct} history={history} />
// 			</Elements>
// 		</StripeProvider>
// 	);
// };

// export default PaymentPage;

class PaymentPage extends React.Component {
	constructor() {
		super();
		this.state = {stripe: null};
		this.selectedProduct = localStorage.getItem("cart");
	}
	componentDidMount() {
		// Create Stripe instance in componentDidMount
		// (componentDidMount only fires in browser/DOM environment)
		this.setState({stripe: window.Stripe("pk_test_51IdHtcBE2zBABmfMjO1abGMinLBL367co4OwBcEZWycAhL4lRtJa4Iab6NSnWFkTlUz6OmrCfaAtWbt3IHLJdkXo00DX0IkCBt")});
	}
	render() {
		return (
			<StripeProvider stripe={this.state.stripe}>
				<Elements>
					<PaymentForm selectedProduct={this.selectedProduct} />
				</Elements>
			</StripeProvider>
		);
	}
}

export default PaymentPage;
