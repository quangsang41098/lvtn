import React, { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom'
import ApiRestaurant from '../../services/restaurant-service'
import ApiUser from '../../services/user-service'
import { CheckOutlined, MailOutlined } from '@ant-design/icons'
import { Table } from 'antd'
import ModalPopup from '../../components/Modal'
import './Statistic.scss'

const Statistic = () => {
    const columns = [
        {
          title: 'Mã đơn hàng',
          dataIndex: 'code',
        },
        {
          title: 'Ngày đặt',
          dataIndex: 'date',
        },
        {
          title: 'Người đặt',
          dataIndex: 'email',
        },
        {
          title: 'Phòng sảnh',
          dataIndex: 'venue',
        },
        {
          title: 'Số người',
          dataIndex: 'numberPeople',
        },
        {
          title: 'Chi tiết',
          dataIndex: 'detail',
        },
        {
          title: 'Status',
          dataIndex: 'status',
        },
        {
          title: 'Action',
          dataIndex: 'action',
            render: (_, record) => {
                return <div className="action" style={{display:'flex'}}>
                    <div><CheckOutlined /></div>
                    {/* <div onClick={(e) => onClickDecline(e, record)}><CloseOutlined /></div> */}
                </div>
            }
        },
    ];
    return (
        <div className="statis-page">
            <h3>Thống kê đặt hàng</h3>
            <Table 
                columns={columns} 
                dataSource={[]} 
                rowClassName="request-page-row" 
                onRow={(record, rowIndex) => {
                    return {
                      onClick: event => { 
                          history.push(`/page-detail/${record.key}`)
                       },
                    };
                }}
            />
            <h3>Doanh thu</h3>
        </div>
    );
}
export default Statistic