
import imgdetail1 from '../../images/detail1.jpg'
import React, { useState } from 'react';
import { Typography, InputNumber, Button, Modal } from 'antd'
import { MinusOutlined, PlusOutlined, WarningOutlined, ShoppingCartOutlined } from "@ant-design/icons";
import { addCartToLocal } from '../../plugins/localstorage'
import './PopupItemDetail.scss'
const { Text } = Typography;
const PopupItemDetail = (props) => {

    const { menuInfo, restaurantInfo, venue, selectedDate } = props
    console.log(venue)
    const [quantity, setQuantity] = useState(Math.round(venue.numberTable.min/menuInfo.numberPeople))
    const AddToCart = () => {
        addCartToLocal(
            {
                restaurant_name: restaurantInfo.name,
                restaurant_id: restaurantInfo._id,
                venueId: menuInfo.venueId[0],
                item_type: "menu",
                selectedDate,
                item: {
                    item_name: menuInfo.name,
                    item_price: menuInfo.price,
                    item_quantity: quantity,
                    item_image: menuInfo.image,
                    item_id: menuInfo._id,
                    item_type: "menu",
                }
            }
        )
        Modal.destroyAll()
    }

    const onChangeQuantity = (value) => {
        console.log(value)
    }
    return (
        <div className="wrap-popup">
            <div className="image">
                <img src={imgdetail1} alt=""/>
            </div>
            <div className="info">
                <h3 className="mt-0">Danh sách món</h3>
                <div className="list-food">
                    <ul>
                        {menuInfo.foodList.map(item => {
                            return <li>{item}</li>
                        })}
                    </ul>
                </div>
            </div>
            <div className="price">
                <h3 className="mt-0">{menuInfo.name}</h3>
                <div className="block quantity">
                    <strong>Phòng sảnh</strong>
                    <p>{venue.name}</p>
                </div>
                <div className="block quantity">
                    <strong>Số lượng</strong>
                    <div className="block-right">
                        <Button type="primary" onClick={() => setQuantity(quantity - 1)}
                                disabled={quantity === Math.round(venue.numberTable.min/menuInfo.numberPeople)}
                        ><MinusOutlined /></Button>
                            <InputNumber 
                                min={Math.round(venue.numberTable.min/menuInfo.numberPeople)} 
                                max={Math.round(venue.numberTable.max/menuInfo.numberPeople)}
                                value={quantity} onChange={onChangeQuantity} />
                        <Button type="primary" onClick={() => setQuantity(quantity + 1)}
                                disabled={quantity === Math.round(venue.numberTable.max/menuInfo.numberPeople)}><PlusOutlined /></Button>
                    </div>
                </div>
                <div className="block quantity">
                    <strong>Số người</strong>
                    <p>{menuInfo.numberPeople}</p>
                </div>
                <div className="block total-price">
                    <strong>Giá sản phẩm</strong>
                    <p>{(menuInfo.price * quantity).toLocaleString('it-IT', {style : 'currency', currency : 'VND'})}</p>

                </div>
                <div className="add-to-cart">
                    <Button type="primary" onClick={AddToCart}><ShoppingCartOutlined />Thêm vào giỏ hàng</Button><br />
                    <Text type="danger" style={{ fontSize: '11px'}}><WarningOutlined />Những menu của phòng sảnh khác sẽ bị xóa</Text>
                </div>
            </div>
        </div>
    )
}

export default PopupItemDetail