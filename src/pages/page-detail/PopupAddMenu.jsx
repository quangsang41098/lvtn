
import React, { useState } from 'react';
import request from 'superagent';
import ApiRestaurant from '../../services/restaurant-service'
import './PopupAddMenu.scss'
import { PlusOutlined, DeleteOutlined } from "@ant-design/icons";
import { Upload, Input, Select, InputNumber, Button, Modal, message } from 'antd';
const { Search } = Input
import ModalPopup from '../../components/Modal'

const optionsType = [
    { label: 'Tiệc cưới', value: "tieccuoi" },
    { label: 'Sinh nhật', value: "sinhnhat" },
    { label: 'Hội thảo/Hội nghị', value: "hoinghi" },
    { label: 'Liên hoan/Tiệc', value: "lienhoan" },
    { label: 'Họp nhóm', value: "hopnhom" },
    { label: 'Workshop', value: "workshop" },
];

const PopupAddMenu = (props) => {
    const { venueList, initRestaurantDetail, rest_id, menuInfo, typeModal } = props
    const [listImage, setListImage] = useState(menuInfo.image ? [{
        uid: 0,
        status: 'done',
        url: menuInfo.image,
      }] : [])
    const [listMenu, setListMenu] = useState(menuInfo.foodList || [])
    const [type, setType] = useState(menuInfo.type)
    const [venue, setVenue] = useState(menuInfo.venueId && menuInfo.venueId[0])
    const [price, setPrice] = useState(menuInfo.price)
    const [name, setName] = useState(menuInfo.name)
    const [numberPeople, setNumberPeople] = useState(menuInfo.numberPeople)
    const dummyRequest = ({ file, onSuccess }) => {
      setTimeout(() => {
        onSuccess("ok");
      }, 0);
    };
    const handleUpload = (file) => {
        const url = `https://api.cloudinary.com/v1_1/driys0e2o/upload`;
        request.post(url)
            .field('upload_preset', 'siv1hj8b')
            .field('file', file.file.originFileObj)
            .field('multiple', true)
            .end((error, response) => {
                if(response.status === 200) {
                    setListImage([...listImage, {
                        uid: listImage.length,
                        status: 'done',
                        url: response.body.url,
                      }])
                }
            });
    }
    const uploadButton = (
        <div>
          <PlusOutlined />
          <div style={{ marginTop: 8 }}>Upload Image</div>
        </div>
      );
    const onAddFood = (value) => {
        setListMenu([...listMenu, value])
    }
    const onClickRemoveFood = (id) => {
        let listFoodTmp = [...listMenu]
        listFoodTmp.splice(id, 1)
        setListMenu(listFoodTmp)
    }
    const addMenu = async () => {
        let formData = {}
        if(name) formData.name = name 
        else return message.error("Vui lòng nhập tên thực đơn")
        if(type) formData.type = type 
        else return message.error("Vui lòng chọn loại hình")
        if(!venue) return message.error("Vui lòng chọn phòng sảnh")
        if(price) formData.price = price 
        else return message.error("Vui lòng nhập giá tiền")
        if(listImage[0]) formData.image = listImage[0] .url
        if(listMenu.length > 0) formData.foodList = listMenu 
        else return message.error("Vui lòng nhập món ăn")
        if(numberPeople) formData.numberPeople = numberPeople 
        else return message.error("Vui lòng nhập số người")
        const response = await ApiRestaurant.createMenu(formData, venue)
        if(response.ok) {
            initRestaurantDetail(rest_id)
            message.success("Thêm menu thành công")
            Modal.destroyAll()
        }
    }
    const editMenu = async () => {
        let formData = {}
        if(name) formData.name = name 
        else return message.error("Vui lòng nhập tên thực đơn")
        if(type) formData.type = type 
        else return message.error("Vui lòng chọn loại hình")
        if(!venue) return message.error("Vui lòng chọn phòng sảnh")
        if(price) formData.price = price 
        else return message.error("Vui lòng nhập giá tiền")
        if(listImage[0]) formData.image = listImage[0] .url
        if(listMenu.length > 0) formData.foodList = listMenu 
        else return message.error("Vui lòng nhập món ăn")
        if(numberPeople) formData.numberPeople = numberPeople 
        else return message.error("Vui lòng nhập số người")
        const response = await ApiRestaurant.EditMenu(formData, menuInfo._id)
        if(response.ok) {
            initRestaurantDetail(rest_id)
            message.success("Sửa menu thành công")
            Modal.destroyAll()
        }
    }
    const deleteMenu = async () => {
        ModalPopup({
            type: "confirm",
            content: () => <div>Are you sure?</div>,
            onOk: async () => {
                const response = await ApiRestaurant.deleteMenu(menuInfo._id)
                if(response.ok) {
                    initRestaurantDetail(rest_id)
                    message.success("Xóa menu thành công")
                    Modal.destroyAll()
                }

            }
        })

    }
    const onChangeType = (value) => {
        setType(value)
    }
    const onChangeVenue = (value) => {
        setVenue(value)
    }
    const onChangePrice = (value) => {
        setPrice(value)
    }
    const onChangeName = (e) => {
        setName(e.target.value)
    }
    const onChangeNumberPeople = (value) => {
        setNumberPeople(value)
    }
    return (
        <React.Fragment>
        <div 
            className="wrap-popup-addmenu"
            style={{
                display: 'flex',
                justifyContent: 'space-between'
            }}
        >
            <Upload
                listType="picture-card"
                fileList={listImage}
                customRequest={dummyRequest}
                onChange={handleUpload}
            >
                {listImage.length !== 1 && uploadButton}
            </Upload>
            
            <div className="block info">
                <h3 className="mt-0">Danh sách món</h3>
                <div className="list-food">
                    <ul>
                        {listMenu.map((item, idx) => {
                        return <li style={{ display: 'flex', justifyContent: 'space-between'}}>
                                    {item}<div style={{ cursor: 'pointer' }} onClick={() => onClickRemoveFood(idx)}><DeleteOutlined /></div>
                                </li>
                        })}
                    </ul>
                    <Search
                        placeholder="Nhập món ăn"
                        enterButton="Add"
                        size="middle"
                        onSearch={onAddFood}
                    />
                </div>
            </div>
            <div className="block room">
                <div className="block-0">
                    <h3 className="mt-0">* Tên thực đơn</h3>
                    <div style={{ display: 'flex', alignItems: 'center', width: '100%'}}>
                        <Input
                            onChange={onChangeName}
                            value={name}
                        ></Input>
                    </div>
                </div>
                <div className="block-0 mt-0">
                    <h3 className="mt-0">* Loại hình</h3>
                    <Select value={type} style={{ width: '100%' }} onChange={onChangeType}>
                        {optionsType.map(type => (
                            <Select.Option value={type.value} key={type.value}>{type.label}</Select.Option>
                        ))}
                    </Select>
                </div>
                <div className="block-1 mt-0">
                    <h3 className="mt-0">* Phòng sảnh</h3>
                    <Select value={venue} style={{ width: '100%' }} onChange={onChangeVenue}>
                        {venueList.map(venue => (
                            <Select.Option value={venue._id} key={venue._id}>{venue.name}</Select.Option>
                        ))}
                    </Select>
                </div>
                <div className="block-2 mt-10">
                    <h3 className="mt-0">* Giá tiền</h3>
                    <div style={{ display: 'flex', alignItems: 'center', width: '100%'}}>
                        <InputNumber
                            value={price}
                            min={1}
                            onChange={onChangePrice}
                            formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                            parser={value => value.replace(/\$\s?|(,*)/g, '')}
                        ></InputNumber>VND
                    </div>
                </div>
                <div className="block-2 mt-0">
                    <h3 className="mt-0">* Số người</h3>
                    <div style={{ display: 'flex', alignItems: 'center', width: '100%'}}>
                        <InputNumber
                            min={1}
                            onChange={onChangeNumberPeople}
                            value={numberPeople}
                        ></InputNumber>
                    </div>
                </div>
            </div>
        </div>
            <div className="btn-group" style={{ marginTop: '20px', display: 'flex', justifyContent: 'flex-end'}}>
                <Button onClick={() => Modal.destroyAll()}>Hủy</Button>
                {typeModal === "edit" && <Button type="danger" style={{ marginLeft: '10px'}} onClick={() => deleteMenu()}>Xóa</Button>}
                {typeModal === "edit" ?
                <Button type="primary" style={{ marginLeft: '10px'}} onClick={editMenu}>Sửa</Button> :
                <Button type="primary" style={{ marginLeft: '10px'}} onClick={addMenu}>Thêm</Button>}
            </div>
        </React.Fragment>
    )
}

export default PopupAddMenu