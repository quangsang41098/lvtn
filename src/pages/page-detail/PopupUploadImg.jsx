
import React, { useState } from 'react';
import request from 'superagent';
// import './PopupItemDetail.scss'
import { PlusOutlined } from "@ant-design/icons";
import { Upload, Button, Modal } from 'antd';
const PopupUploadImg = (props) => {
    const { updateRest } = props
    const [listImage, setListImage] = useState(props.listImage)
    const dummyRequest = ({ file, onSuccess }) => {
      setTimeout(() => {
        onSuccess("ok");
      }, 0);
    };
    const handleUpload = (file) => {
        const url = `https://api.cloudinary.com/v1_1/driys0e2o/upload`;
        request.post(url)
            .field('upload_preset', 'siv1hj8b')
            .field('file', file.file.originFileObj)
            .field('multiple', true)
            .end((error, response) => {
                if(response.status === 200) {
                    setListImage([...listImage, {
                        uid: listImage.length,
                        status: 'done',
                        url: response.body.url,
                      }])
                }
            });
    }
    const uploadButton = (
        <div>
          <PlusOutlined />
          <div style={{ marginTop: 8 }}>Upload</div>
        </div>
      );
    const saveImg = () => {
      updateRest({ listImage: listImage.map(e => e.url)})

      Modal.destroyAll()
    }
    return (
        <div className="wrap-popup-upload">
            <Upload
                listType="picture-card"
                fileList={listImage}
                customRequest={dummyRequest}
                onChange={handleUpload}
            >
                {uploadButton}
            </Upload>
            <div className="btn-group" style={{ marginTop: '20px', display: 'flex', justifyContent: 'flex-end'}}>
                <Button onClick={() => Modal.destroyAll()}>Cancel</Button>
                <Button type="primary" style={{ marginLeft: '10px'}} onClick={saveImg}>Lưu</Button>
            </div>
        </div>
    )
}

export default PopupUploadImg