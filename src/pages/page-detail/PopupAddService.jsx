
import React, { useState, useEffect, useContext } from 'react';
import UserContext from '../../context/info-context'
import ApiRestaurant from '../../services/restaurant-service'
import './PopupAddService.scss'
import { ShoppingCartOutlined, DeleteOutlined } from "@ant-design/icons";
import { Upload, Input, Select, InputNumber, Button, Modal, message } from 'antd';
import { addCartToLocal } from '../../plugins/localstorage'
const { Search } = Input
import ModalPopup from '../../components/Modal'
import { remove } from "lodash"

const optionsType = [
    { label: 'Tiệc cưới', value: "tieccuoi" },
    { label: 'Sinh nhật', value: "sinhnhat" },
    { label: 'Hội thảo/Hội nghị', value: "hoinghi" },
    { label: 'Liên hoan/Tiệc', value: "lienhoan" },
    { label: 'Họp nhóm', value: "hopnhom" },
    { label: 'Workshop', value: "workshop" },
];

const PopupAddMenu = (props) => {
    const { initRestaurantDetail, restaurantInfo, service_info } = props
    const { userInfo } = useContext(UserContext)
    const rest_id = restaurantInfo._id
    const [listService, setListService] = useState([])
    const [selectedService, setSelectedService] = useState(null)
    const addMenu = async () => {
        let formData = {}
        const listServiceRest = restaurantInfo.services || []
        const listIdServiceRest = listServiceRest && listServiceRest.map(e => e._id)
        if(listIdServiceRest.includes(selectedService._id)) return message.error("Dịch vụ đã có sẵn. Vui lòng chọn dịch vụ khác!")
        formData.services = [...listServiceRest, selectedService]
        const response = await ApiRestaurant.updateRestaurant(formData, rest_id)
        if(response.ok) {
            initRestaurantDetail(rest_id)
            message.success("Thêm dịch vụ thành công")
            Modal.destroyAll()
        }
    }
    const deleteService = async () => {
        ModalPopup({
            type: "confirm",
            content: () => <div>Are you sure?</div>,
            onOk: async () => {
                const listServiceRest = restaurantInfo.services || []
                remove(listServiceRest, (e => e._id === service_info._id))
                const response = await ApiRestaurant.updateRestaurant({ services: listServiceRest}, rest_id)
                if(response.ok) {
                    initRestaurantDetail(rest_id)
                    message.success("Xóa dịch vụ thành công")
                    Modal.destroyAll()
                }

            }
        })

    }
    const AddToCart = () => {
        addCartToLocal(
            {
                restaurant_name: restaurantInfo.name,
                restaurant_id: restaurantInfo._id,
                item_type: "service",
                item: {
                    item_name: service_info.name,
                    item_price: service_info.price,
                    item_quantity: 1,
                    item_image: service_info.images[0],
                    item_id: service_info._id,
                    item_type: "service",
                }
            }
        )
        Modal.destroyAll()
    }
    const handleChangeService = (value) => {
        setSelectedService(listService.find(e => e._id === value))
    }
    const initListService = async () => {
        const response = await ApiRestaurant.getService()
        if(response.ok) {
            setListService(response.data)
        }
    }
    useEffect(() => {
        initListService()
        if(service_info) setSelectedService(service_info)
    }, [])
    return (
        <React.Fragment>
        <div 
            className="wrap-popup-addservice"
        >
            
            <div className="block room">
                <div className="block-0">
                    <h3 className="mt-0">Dịch vụ</h3>
                    {!service_info ? <Select style={{ width: '100%' }} onChange={handleChangeService}>
                        {listService.map((e, idx) => {
                            return <Option value={e._id} key={e._id}>{e.name}</Option>
                        })}
                    </Select> :
                    <h3 style={{ color: '#fcae02!important'}}>{service_info.name}</h3>
                    }
                </div>
                {selectedService && <div className="block-1 image">
                    <img src={selectedService.images[0]} alt="" />
                </div>}
                {selectedService && <div className="">
                    <h3 className="mt-0">Mô tả</h3>
                    <p>{selectedService.description}</p>
                </div>}
                {selectedService && <div className="block-2 mt-10">
                    <h3 className="mt-0">Giá tiền</h3>
                    <p style={{ color: '#fcae02'}}>{selectedService.price.toLocaleString('it-IT', {style : 'currency', currency : 'VND'})}</p>
                </div>}
            </div>
        </div>
        <div className="btn-group" style={{ marginTop: '20px', display: 'flex', justifyContent: 'flex-end'}}>
            <Button onClick={() => Modal.destroyAll()}>Hủy</Button>
            {["rest_admin", "sys_admin"].includes(userInfo) ? 
                service_info ? 
                    <Button type="danger" style={{ marginLeft: '10px'}} onClick={() => deleteService()}>Xóa</Button>:
                    <Button type="primary" style={{ marginLeft: '10px'}} onClick={addMenu}>Thêm</Button> 
                :
                <Button type="primary"style={{ marginLeft: '10px'}} onClick={AddToCart}><ShoppingCartOutlined />Thêm vào giỏ hàng</Button>
            }
        </div>
        </React.Fragment>
    )
}

export default PopupAddMenu