
import imgdetail1 from '../../images/detail1.jpg'
import React, { useState } from 'react';
import { Checkbox, InputNumber, Button, Modal } from 'antd'
import { MinusOutlined, PlusOutlined, DeleteOutlined, ShoppingCartOutlined } from "@ant-design/icons";
import { addCartToLocal } from '../../plugins/localstorage'
import './PopupItemDetail.scss'
const PopupItemDetailDrink = (props) => {

    const { menuInfo, restaurantInfo, type } = props

    const [quantity, setQuantity] = useState(1)
    const AddToCart = () => {
        addCartToLocal(
            {
                restaurant_name: restaurantInfo.name,
                item: {
                    item_name: menuInfo.name,
                    item_price: menuInfo.price,
                    item_quantity: quantity
                }
            }
        )
        Modal.destroyAll()
    }

    const onChangeQuantity = (value) => {
    }
    return (
        <div className="wrap-popup">
            <div className="image">
                <img src={imgdetail1} alt=""/>
            </div>
            <div className="price">
                <h3 className="mt-0">{menuInfo.name}</h3>
                <div className="block quantity">
                    <p>Số lượng</p>
                    <div className="block-right">
                        <Button type="primary" onClick={() => setQuantity(quantity - 1)}><MinusOutlined /></Button>
                            <InputNumber min={1} max={1000} value={quantity} onChange={onChangeQuantity} />
                        <Button type="primary" onClick={() => setQuantity(quantity + 1)}><PlusOutlined /></Button>
                    </div>
                </div>
                <div className="block total-price">
                    <p>Giá sản phẩm</p>
                    <p>{(menuInfo.price * quantity).toLocaleString('it-IT', {style : 'currency', currency : 'VND'})}</p>

                </div>
                <div className="add-to-cart">
                    <Button type="primary" onClick={AddToCart}><ShoppingCartOutlined />Thêm vào giỏ hàng</Button>
                </div>
            </div>
        </div>
    )
}

export default PopupItemDetailDrink