
import React, { useState } from 'react';
import request from 'superagent';
// import './PopupItemDetail.scss'
import { PlusOutlined } from "@ant-design/icons";
import { Input, Button, Modal, Checkbox } from 'antd';
const { TextArea } = Input
const optionsType = [
    { label: 'Tiệc cưới', value: "tieccuoi" },
    { label: 'Sinh nhật', value: "sinhnhat" },
    { label: 'Hội thảo/Hội nghị', value: "hoinghi" },
    { label: 'Liên hoan/Tiệc', value: "lienhoan" },
    { label: 'Họp nhóm', value: "hopnhom" },
    { label: 'Workshop', value: "workshop" },
];
const PopupEditIntro = (props) => {
    const { updateRest } = props
    const [events, setEvents] = useState(props.events)
    
    const editText = () => {
        updateRest({
            providedEvent: events
        })
        Modal.destroyAll()
    }
    
    const onChangeCheckbox = (value) => {
        setEvents(value)
    }
    return (
        <div className="wrap-popup-edittext">
            
            <div className="listEvent">
                <Checkbox.Group options={optionsType} value={events} name="providedEvent" onChange={onChangeCheckbox} />

            </div>
            
            <div className="btn-group" style={{ marginTop: '20px', display: 'flex', justifyContent: 'flex-end'}}>
                <Button onClick={() => Modal.destroyAll()}>Hủy</Button>
                <Button type="primary" style={{ marginLeft: '10px'}} onClick={editText}>Thêm</Button>
            </div>
        </div>
    )
}

export default PopupEditIntro