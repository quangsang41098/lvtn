import React, { useRef, useState, useEffect, useContext } from 'react';
import { useParams } from 'react-router-dom'
import ApiRestaurant from '../../services/restaurant-service'
import imgdetail1 from '../../images/detail1.jpg'
import './PageDetail.scss'
import ModalPopup from '../../components/Modal'
import { Button, Checkbox, Carousel, DatePicker, Tag  } from 'antd';
import PopupItemDetail from './PopupItemDetail'
import PopupItemDetailDrink from './PopupItemDetailDrink'
import PopupAddService from './PopupAddService'
import { EnvironmentOutlined, GlobalOutlined, FundProjectionScreenOutlined, ScheduleOutlined, GiftOutlined, HeartOutlined } from "@ant-design/icons";
import Mapbox from '../../components/Mapbox'
import { get, remove } from "lodash"
import Icon from '../../images/icon/index.js'
import Restaurant from '../../components/restaurant';
import moment from 'moment';

import UserContext from '../../context/info-context'

const typeMapping = {
    'tieccuoi': 'Tiệc cưới',
    'sinhnhat': 'Sinh nhật',
    'hoinghi': 'Hội nghị/Hội thảo',
    'lienhoan': 'Liên hoan/Tiệc',
    'hopnhom': 'Họp nhóm',
    'workshop': 'Workshop'
}
const propertiesMapping = {
    "bantron": "Bàn tròn",
    "ngoaitroi": "Ngoài trời",
    "sankhau": "Sân khấu",
    "bancong": "Ban công",
    "tiecdung": "Tiệc đứng"
}
const iconMapping = {
    'tieccuoi': <HeartOutlined className="icon-event" />,
    'sinhnhat': <GiftOutlined className="icon-event" />,
    'hoinghi': <GlobalOutlined className="icon-event" />,
    'lienhoan': <img src={Icon.hopnhom} />,
    'hopnhom': <ScheduleOutlined className="icon-event" />,
    'workshop': <FundProjectionScreenOutlined className="icon-event" />
}
const list_menus = [1,2,3,4]
const PageDetail = (props) => {
    const params = useParams()
    const userContext = useContext(UserContext)
    const [listVenueCheckbox, setListVenueCheckBox] = useState([])
    const [listService, setListService] = useState([])
    const [rangePrice, setRangePrice] = useState([0, 0])
    const [listRestaurant, setListRestaurant] = useState([])
    const [restaurantInfo, setRestaurantInfo] = useState({
        description: '',
        providedEvent: [],
        menuList: [],
        address: {
            detail: '',
            district: '',
            ward: ''
        },
        listImage: []
    })
    const [selectedDate, setSelectDate] = useState(null)
    const CarouselRef = useRef()
    const showModalMenu = (menuInfo, venue) => {
        ModalPopup({
            type: "confirm",
            content: PopupItemDetail,
            propsContent: { menuInfo, restaurantInfo, venue, selectedDate },
            className: "ant-modal-menu"
        })
    }
    const showModalDrink = (menuInfo) => {
        ModalPopup({
            type: "confirm",
            content: PopupItemDetailDrink,
            propsContent: { menuInfo, restaurantInfo },
            className: "ant-modal-drink"
        })
    }
    const showService = (service_info) => {
        ModalPopup({
            type: "empty",
            content: PopupAddService,
            propsContent: { 
                restaurantInfo,
                initRestaurantDetail,
                service_info
             },
            className: "ant-modal-addservice"
        })
    }
    const onClickImage = (idx) => {
        CarouselRef.current.goTo(idx, false)
    }

    const initRestaurantDetail = async (id) => {
        const response = await ApiRestaurant.getDetailRestaurant(id)
        if(response.ok) {
            let listRes = response.data.restaurant
            setRestaurantInfo(listRes)
            setListVenueCheckBox([listRes.venueList.map(e => e._id)[0]])
            let listPrice = listRes.venueList.map(v => {
                return [...v.menus.map(m => m.price)]
            }).flat()
            console.log([Math.min(...listPrice), Math.max(...listPrice)])
            setRangePrice([Math.min(...listPrice), Math.max(...listPrice)])
        }
    }

    const onChangeCheckVenue = (e, venueId) => {
        const check = e.target.checked
        if(listVenueCheckbox.includes(venueId)) {
            setListVenueCheckBox(listVenueCheckbox.filter(item => item !== venueId))
        } else {
            setListVenueCheckBox([venueId])
        }
    }
    const initListService = async () => {
        const res = await ApiRestaurant.getService()
        if(res.ok) {
            setListService(res.data)
        }
    }
    const initListRestaurant = async () => {
      const response = await ApiRestaurant.getListRestaurant()
      if(response.ok) {
        setListRestaurant(response.data.restaurants.splice(0,4))
      }
    }
    useEffect(() => {
        const { id } = params
        initRestaurantDetail(id)
    },[params])

    useEffect(() => {
        initListService()
        initListRestaurant()
    },[])
    const renderImages = () => {
        const { listImage } = restaurantInfo
        return listImage.length > 0 ? listImage.map(image => {
            return (
                <div style={{backgroundColor: "#FFF"}}>
                    <img src={image} alt=""/>
                </div>
            )
        }) : 
        <div style={{backgroundColor: "#FFF"}}>
            <img src={imgdetail1} alt=""/>
        </div>
    }
    const disabledDate = (current, reserved) => {
        return (current && current < moment().endOf('day')) || reserved.includes(current.format("DD MM YYYY"));
      }
    const onChangeDate = (v) => {
        setSelectDate(v.unix())
    }
    return (
        <div className="wrap-detail">
            <div className="wrap-detail__info">
                <div className="wrap-detail__info-left">
                    <div className="list-image">
                        <div className="main-image">
                            <Carousel ref={CarouselRef}>
                               {renderImages()}
                            </Carousel>
                        </div>
                        <div className="slider">
                            {
                                restaurantInfo.listImage.length > 0 ? restaurantInfo.listImage.map((path, idx) => {
                                    return (
                                        <img key={idx} src={path} alt=""  onClick={() => onClickImage(idx)} />
                                    )
                                }) : 
                                <img key={0} src={imgdetail1} alt=""  onClick={() => onClickImage(0)} />
                            }
                        </div>
                    </div>
                    <div className="introduce">
                        <div className="title">
                            <h3>Giới thiệu</h3>
                        </div>
                        <p>{restaurantInfo.description}</p>
                    </div>
                    <div className="event-category">
                        <div className="list-event-header">
                            <h3>Loại sự kiện cung cấp</h3>
                        </div>
                        <div className="event-category-list">
                            {restaurantInfo.providedEvent.map(e => {
                                return typeMapping[e] && <p>
                                    {iconMapping[e]} {typeMapping[e]}
                                </p>
                            })}
                        </div>
                    </div>
                    <div className="list-room">
                        <div className="list-room-header">
                            <h3>Các phòng sảnh</h3>
                        </div>
                        <div className="rooms">
                            {restaurantInfo.venueList&&restaurantInfo.venueList.map(venue=>{
                                return (
                                    <div className="item">
                                        <div className="image">
                                            <img src={venue.image||imgdetail1} alt=""/>
                                        </div>
                                        <div className="info">
                                            <h5>{venue.name}</h5>
                                            <p>Số người: {venue.numberTable.min} - {venue.numberTable.max}</p>
                                            <p>Loại sự kiện: {venue.type.map(e => typeMapping[e]).join(',')}</p>
                                            <p>Tiện ích: {venue.properties.map(e => propertiesMapping[e] && <Tag color="orange">{propertiesMapping[e]}</Tag>)}</p>
                                            <p>Chọn ngày tổ chức: 
                                                <DatePicker 
                                                    disabledDate={(current) => disabledDate(current, venue.reserved)} 
                                                    disabled={!listVenueCheckbox.includes(venue._id)}
                                                    onChange={onChangeDate}/>
                                            </p>
                                        </div>
                                        <div className="item__checkbox">
                                            <Checkbox onChange={(e) => onChangeCheckVenue(e, venue._id)} checked={listVenueCheckbox.includes(venue._id)}></Checkbox>
                                        </div>
                                    </div>)
                            })}
                        </div>
                    </div>
                </div>
                <div className="wrap-detail__info-right">
                    <div className="side-address">
                        <h1 className="name-res">{restaurantInfo.name}</h1>
                        <div>
                            <p className="address">
                                <EnvironmentOutlined /> {`${restaurantInfo.address}`}
                            </p>
                        </div>
                        <div className="capa-price">
                            <div className="capa-price__block capa">
                                <p>Sức chứa tối đa: {restaurantInfo.capacity} Người</p>
                            </div>
                            <div className="capa-price__block price">
                                <p>Khoảng giá: {rangePrice[0].toLocaleString('it-IT')} - {rangePrice[1].toLocaleString('it-IT')} VND / menu</p>
                            </div>
                        </div>

                    </div>
                    <div className="side-service">
                        <h3>Gợi ý sản phẩm & dịch vụ</h3>
                        <p>Các loại sản phẩm & dịch vụ khác cho sự kiện của bạn</p>
                        <div className="list-services">
                            {listService.map((el, idx) => {
                                return <div className="item" onClick={() => showService(el)}>
                                            <img src={el.images[0]} alt=""/>
                                            <h4>{el.name}</h4>
                                            <p>Giá sản phẩm</p>
                                            <p className="price">{el.price.toLocaleString('it-IT', {style : 'currency', currency : 'VND'})}</p>
                                        </div>
                                })
                            }
                        </div>
                    </div>
                </div>
            </div>
            <div className="list-menu">
                <div className="list-menu-header">
                    <h3>Danh sách món ăn</h3>
                </div>
                <div className="menus">
                    {restaurantInfo.venueList && restaurantInfo.venueList.map((venue, idx) => {
                        return listVenueCheckbox.includes(venue._id) && venue.menus.map(el => {
                            return <div className="item">
                                        <div className="image">
                                            <img src={el.image||"https://www.sassyhongkong.com/wp-content/uploads/2019/05/vegan-restaurants-veggie-veg-hk-kowloon.jpg"} alt=""/>
                                        </div>
                                        <div className="info">
                                            <h4>{el.name}</h4>
                                            <p>Số người: {el.numberPeople}</p>
                                            <p className="m-0">Giá sản phẩm</p>
                                            <p>{el.price.toLocaleString('it-IT', {style : 'currency', currency : 'VND'})}</p>
                                            <Button className="btn-primary" onClick={() => showModalMenu(el, venue)}>Xem chi tiết</Button>
                                        </div>
                                    </div>
                            })

                        })
                    }
                    
                </div>
            </div>
            {/* <div className="list-drink">
                <h3>Danh sách đồ uống</h3>
                <div className="drinks">
                    {restaurantInfo.drinks && restaurantInfo.drinks.map((el, idx) => {
                        return <div className="item" key={idx}>
                                    <div className="image">
                                        <img src={el.image||"https://www.sassyhongkong.com/wp-content/uploads/2019/05/vegan-restaurants-veggie-veg-hk-kowloon.jpg"} alt=""/>
                                    </div>
                                    <div className="info">
                                        <h4>{el.name}</h4>
                                        <p className="m-0">Giá sản phẩm</p>
                                        <p>{el.price.toLocaleString('it-IT', {style : 'currency', currency : 'VND'})} đ</p>
                                        <Button className="btn-primary" onClick={() => showModalDrink(el)}>Xem chi tiết</Button>
                                    </div>
                                </div>
                        })
                    }
                    
                </div>

            </div> */}
            <div className="wrap-map">
                <h3>THÔNG TIN VỊ TRÍ</h3>
                <Mapbox coordinates={get(restaurantInfo , "location.coordinates")}/>
            </div>
            <div className="rest-recommend">
                <h3>Các nhà hàng khác</h3>
                <div className="items">
                    {listRestaurant.map((infores) =>{
                        return <Restaurant info={infores} />
                    })}
                </div>

            </div>
        </div>
    ) 
};
export default PageDetail;
