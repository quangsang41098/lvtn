
import React, { useState, useEffect, useContext } from 'react';
import request from 'superagent';
import ApiRestaurant from '../../services/restaurant-service'
import './PopupAddVenue.scss'
import { PlusOutlined, DeleteOutlined } from "@ant-design/icons";
import { Upload, Input, Select, InputNumber, Button, Modal, message, Checkbox } from 'antd';
import UserContext from '../../context/info-context'
import ModalPopup from '../../components/Modal'

const optionsType = [
    { label: 'Tiệc cưới', value: "tieccuoi" },
    { label: 'Sinh nhật', value: "sinhnhat" },
    { label: 'Hội thảo/Hội nghị', value: "hoinghi" },
    { label: 'Liên hoan/Tiệc', value: "lienhoan" },
    { label: 'Họp nhóm', value: "hopnhom" },
    { label: 'Workshop', value: "workshop" },
];
const listLtilities = ["Tiệc trong nhà", "Tiệc ngoài trời" ]
const PopupAddMenu = (props) => {
    const { rest_id, initRestaurantDetail, venue_info, userInfo } = props
    const { role } = userInfo
    const [listImage, setListImage] = useState([
        {
            uid: 0,
            status: 'done',
            url: venue_info.image,
        }] || [])
    const [numberTable, setNumberTable] = useState({
        min: venue_info.numberTable.min || 0,
        max: venue_info.numberTable.max || 0
    })
    const [name, setName] = useState(venue_info.name || '')
    const [type, setType] = useState(venue_info.type || [])
    const [utilities, setUtilities] = useState([]);
    const [utilitiesSelect, setUtilitiesSelect] = useState(venue_info.properties || []);
    const dummyRequest = ({ file, onSuccess }) => {
      setTimeout(() => {
        onSuccess("ok");
      }, 0);
    };
    const handleUpload = (file) => {
        const url = `https://api.cloudinary.com/v1_1/driys0e2o/upload`;
        request.post(url)
            .field('upload_preset', 'siv1hj8b')
            .field('file', file.file.originFileObj)
            .field('multiple', true)
            .end((error, response) => {
                if(response.status === 200) {
                    setListImage([...listImage, {
                        uid: listImage.length,
                        status: 'done',
                        url: response.body.url,
                      }])
                }
            });
    }
    const uploadButton = (
        <div>
          <PlusOutlined />
          <div style={{ marginTop: 8 }}>Upload Image</div>
        </div>
    );
    const onChangeCheckbox = (value) => {
        setType(value)
    }
    const addMenu = async () => {
        let formData = {}
        if(name) formData.name = name 
        else return message.error("Vui lòng nhập tên phòng sảnh")
        if(type.length > 0) formData.type = type 
        else return message.error("Vui lòng chọn loại sự kiện")
        if(listImage[0]) formData.image = listImage[0].url
        if(numberTable.max && numberTable.min) formData.numberTable = numberTable 
        else return message.error("Vui lòng nhập số bàn")
        formData.properties = utilitiesSelect
        const response = await ApiRestaurant.EditVenue(formData, venue_info._id)
        if(response.ok) {
            initRestaurantDetail(rest_id)
            message.success("Thêm phòng sảnh thành công")
            Modal.destroyAll()
        }
    }
    const deleteMenu = async (venueId) => {
        ModalPopup({
            type: "confirm",
            content: () => <div>Are you sure?</div>,
            onOk: async () => {
                const response = await ApiRestaurant.deleteVenue(venueId)
                if(response.ok) {
                    initRestaurantDetail(rest_id)
                    message.success("Xóa phòng sảnh thành công")
                    Modal.destroyAll()
                }

            }
        })

    }
    const onChangeNumberTable = (value, type) => {
        if(type === 'min') setNumberTable({...numberTable, min: value})
        if(type === 'max') setNumberTable({...numberTable, max: value})
    }
    const onChangeName = (e) => {
        setName(e.target.value)
    }
    const handleChangeUtilites = (value) => {
        setUtilitiesSelect(value)
    }
    
    const initListProperties = async () => {
        const res = await ApiRestaurant.getProperties()
        if(res.ok) {
            setUtilities(res.data)
        }
    }
    useEffect(() => {
        initListProperties()
    }, [])
    return (
        <React.Fragment>
        <div 
            className="wrap-popup-addvenue"
            style={{
                display: 'flex',
                justifyContent: 'space-between'
            }}
        >
            <Upload
                listType="picture-card"
                fileList={listImage}
                customRequest={dummyRequest}
                onChange={handleUpload}
            >
                {listImage.length !== 1 && uploadButton}
            </Upload>
            
            <div className="block room">
                <div className="block-0">
                    <h3 className="mt-0">* Tên phòng sảnh</h3>
                    <div style={{ display: 'flex', alignItems: 'center', width: '100%'}}>
                        <Input
                            onChange={onChangeName}
                            value={name}
                        ></Input>
                    </div>
                </div>
                <div className="block-2 mt-10">
                    <h3 className="mt-0">* Số bàn</h3>
                    <div style={{ display: 'flex', alignItems: 'center', width: '100%'}}>
                        <InputNumber
                            placeholder="Min"
                            min={1}
                            value={numberTable.min}
                            onChange={(e) => onChangeNumberTable(e, 'min')}
                        ></InputNumber>
                        <InputNumber
                            placeholder="Max"
                            min={1}
                            value={numberTable.max}
                            onChange={(e) => onChangeNumberTable(e, 'max')}
                        ></InputNumber>
                    </div>
                </div>
                <div className="block-3 mt-10">
                    <h3 className="mt-0">* Loại sự kiện</h3>
                    <Checkbox.Group value={type} options={optionsType}  name="providedEvent" onChange={onChangeCheckbox} />
                </div>
                
                <div className="block-4 mt-10">
                    <h3>Tiện ích</h3>
                    <Select value={utilitiesSelect} mode="tags" style={{ width: '100%' }} onChange={handleChangeUtilites} tokenSeparators={[',']}>
                        {utilities.map((e, idx) => {
                            return <Option key={e}>{e}</Option>
                        })}
                    </Select>
                </div>
            </div>
        </div>
            <div className="btn-group" style={{ marginTop: '20px', display: 'flex', justifyContent: 'flex-end'}}>
                <Button onClick={() => Modal.destroyAll()}>Hủy</Button>
                <Button type="danger" style={{ marginLeft: '10px'}} onClick={() => deleteMenu(venue_info._id)}>Xóa</Button>
                <Button type="primary" style={{ marginLeft: '10px'}} onClick={addMenu}>Sửa</Button>
            </div>
        </React.Fragment>
    )
}

export default PopupAddMenu