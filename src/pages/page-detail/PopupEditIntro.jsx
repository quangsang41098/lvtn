
import React, { useState } from 'react';
import request from 'superagent';
// import './PopupItemDetail.scss'
import { PlusOutlined } from "@ant-design/icons";
import { Input, Button, Modal } from 'antd';
const { TextArea } = Input
const PopupEditIntro = (props) => {
    const { text, updateRest } = props
    const [textEdit, setTextEdit] = useState(text)
    
    const editText = () => {
        updateRest({ description: textEdit})
        Modal.destroyAll()
    }
    const onChangeText = (e) => {
        setTextEdit(e.target.value)
    }
    return (
        <div className="wrap-popup-edittext">
            
            <TextArea
                value={textEdit}
                autoSize={true}
                onChange={onChangeText}
            >
            </TextArea>
            
            <div className="btn-group" style={{ marginTop: '20px', display: 'flex', justifyContent: 'flex-end'}}>
                <Button onClick={() => Modal.destroyAll()}>Hủy</Button>
                <Button type="primary" style={{ marginLeft: '10px'}} onClick={editText}>Thêm</Button>
            </div>
        </div>
    )
}

export default PopupEditIntro