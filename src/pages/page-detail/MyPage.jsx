import React, { useRef, useState, useEffect, useContext } from 'react';
import { useParams } from 'react-router-dom'
import ApiRestaurant from '../../services/restaurant-service'
import imgdetail1 from '../../images/detail1.jpg'
import './PageDetail.scss'
import ModalPopup from '../../components/Modal'
import { Button, Checkbox, Carousel, Tag, message  } from 'antd';
import PopupItemDetail from './PopupItemDetail'
import PopupItemDetailDrink from './PopupItemDetailDrink'
import PopupUploadImg from './PopupUploadImg'
import PopupAddMenu from './PopupAddMenu'
import PopupAddService from './PopupAddService'
import PopupAddVenue from './PopupAddVenue'
import PopupEditVenue from './PopupEditVenue'
import PopupEditIntro from './PopupEditIntro'
import PopupEditEvent from './PopupEditEvent'
import { EnvironmentOutlined, EditOutlined, GlobalOutlined, FundProjectionScreenOutlined, ScheduleOutlined, GiftOutlined, HeartOutlined } from "@ant-design/icons";
import Mapbox from '../../components/Mapbox'
import UserContext from '../../context/info-context'
import Icon from '../../images/icon/index.js'
import { get } from "lodash"

const typeMapping = {
    'tieccuoi': 'Tiệc cưới',
    'sinhnhat': 'Sinh nhật',
    'hoinghi': 'Hội nghị/Hội thảo',
    'lienhoan': 'Liên hoan/Tiệc',
    'hopnhom': 'Họp nhóm',
    'workshop': 'Workshop'
}
const iconMapping = {
    'tieccuoi': <HeartOutlined className="icon-event" />,
    'sinhnhat': <GiftOutlined className="icon-event" />,
    'hoinghi': <GlobalOutlined className="icon-event" />,
    'lienhoan': <img src={Icon.hopnhom} />,
    'hopnhom': <ScheduleOutlined className="icon-event" />,
    'workshop': <FundProjectionScreenOutlined className="icon-event" />
}
const PageDetail = (props) => {
    const params = useParams()
    const userContext = useContext(UserContext)
    const [restaurantInfo, setRestaurantInfo] = useState({
        description: '',
        providedEvent: [],
        menuList: [],
        address: {
            detail: '',
            district: '',
            ward: ''
        },
        listImage: []
    })
    const [listVenueCheckbox, setListVenueCheckBox] = useState([])
    const [listService, setListService] = useState([])
    const CarouselRef = useRef()
    const showModalMenu = (menuInfo) => {
        ModalPopup({
            type: "empty",
            content: PopupAddMenu,
            propsContent: { 
                venueList: restaurantInfo.venueList,
                rest_id: restaurantInfo._id,
                initRestaurantDetail,
                menuInfo,
                typeModal: 'edit'
             },
            className: "ant-modal-addmenu"
        })
    }
    const showModalDrink = (menuInfo) => {
        ModalPopup({
            type: "confirm",
            content: PopupItemDetailDrink,
            propsContent: { menuInfo, restaurantInfo },
            className: "ant-modal-drink"
        })
    }
    const addMenu = (menuInfo) => {
        ModalPopup({
            type: "empty",
            content: PopupAddMenu,
            propsContent: { 
                venueList: restaurantInfo.venueList,
                rest_id: restaurantInfo._id,
                initRestaurantDetail,
                menuInfo: {}
             },
            className: "ant-modal-addmenu"
        })
    }
    const showService = (service_info) => {
        ModalPopup({
            type: "empty",
            content: PopupAddService,
            propsContent: { 
                restaurantInfo,
                initRestaurantDetail,
                service_info
             },
            className: "ant-modal-addservice"
        })
    }
    const addVenue = () => {
        ModalPopup({
            type: "empty",
            content: PopupAddVenue,
            propsContent: { 
                rest_id: restaurantInfo._id,
                initRestaurantDetail
            },
            className: "ant-modal-addvenue"
        })
    }
    const showVenue = (venue) => {
        ModalPopup({
            type: "empty",
            content: PopupEditVenue,
            propsContent: { 
                venue_info: venue,
                rest_id: restaurantInfo._id,
                initRestaurantDetail,
                userInfo: userContext.userInfo
            },
            className: "ant-modal-addvenue"
        })
    }
    
    const onChangeCheckVenue = (e, venueId) => {
        e.stopPropagation();
        const check = e.target.checked
        if(listVenueCheckbox.includes(venueId)) {
            setListVenueCheckBox(listVenueCheckbox.filter(item => item !== venueId))
        } else {
            setListVenueCheckBox([...listVenueCheckbox, venueId])
        }
    }
    const addEvent = () => {
        return ModalPopup({
            type: "empty",
            title: "Các sự kiện",
            content: PopupEditEvent,
            propsContent: {
                events: restaurantInfo.providedEvent,
                updateRest
            }
        })
    }
    const onClickImage = (idx) => {
        CarouselRef.current.goTo(idx, false)
    }

    const editIntroduce = (text) => {
        return ModalPopup({
            type: "empty",
            title: "Giới thiệu",
            content: PopupEditIntro,
            propsContent: {
                text,
                updateRest
            }
        })
    }
    const editImg = (text) => {
        ModalPopup({
            type: "empty",
            title: "Hình ảnh",
            okText: "Lưu",
            content: PopupUploadImg,
            propsContent: {
                listImage: restaurantInfo.listImage.map((e, idx) => {
                    return {
                      uid: idx,
                      status: 'done',
                      url: e,
                    }
                }),
                updateRest
            }
        })
    }

    const initRestaurantDetail = async (id) => {
        const response = await ApiRestaurant.getDetailRestaurant(id)
        if(response.ok) {
            let listRes = response.data.restaurant
            setRestaurantInfo(listRes)
            setListVenueCheckBox(listRes.venueList.map(e => e._id))
        }
    }
    const updateRest = async (data) => {
        let formData = {}
        if(!data) return;
        if(data.listImage) formData.listImage = data.listImage
        if(data.description) formData.description = data.description
        if(data.providedEvent) formData.providedEvent = data.providedEvent
        const response = await ApiRestaurant.updateRestaurant(formData, restaurantInfo._id)
        if(response.ok) {
            message.success("Cập nhật thành công")
            initRestaurantDetail(restaurantInfo._id)
        } else message.error("Error")
    }
    const initListService = async () => {
        const res = await ApiRestaurant.getService()
        if(res.ok) {
            setListService(res.data)
        }
    }
    useEffect(() => {
        const { id } = params
        if(userContext.userInfo.role === "rest_admin") {
            initRestaurantDetail(userContext.userInfo.info.restAdmin[0])
        }
        initListService()
    },[])

    const renderImages = () => {
        const { listImage } = restaurantInfo
        return listImage.length > 0 ? listImage.map(image => {
            return (
                <div style={{backgroundColor: "#FFF"}}>
                    <img src={image} alt=""/>
                </div>
            )
        }) : 
        <div style={{backgroundColor: "#FFF"}}>
            <img src={imgdetail1} alt=""/>
        </div>
    }
    return (
        <div className="wrap-detail">
            <div className="wrap-detail__info">
                <div className="wrap-detail__info-left">
                    <div className="list-image">
                        <div className="main-image">
                            <Carousel ref={CarouselRef}>
                               {renderImages()}
                            </Carousel>
                        </div>
                        <div className="add-img" onClick={() => editImg()}>
                            <EditOutlined /><span>Chỉnh sửa</span>
                        </div>
                        <div className="slider">
                        {
                                restaurantInfo.listImage.length > 0 ? restaurantInfo.listImage.map((path, idx) => {
                                    return (
                                        <img key={idx} src={path} alt=""  onClick={() => onClickImage(idx)} />
                                    )
                                }) : 
                                <img key={0} src={imgdetail1} alt=""  onClick={() => onClickImage(0)} />
                            }
                        </div>
                    </div>
                    <div className="introduce">
                        <div className="title">
                            <h3>Giới thiệu</h3>
                            <div className="edit" onClick={() => editIntroduce(restaurantInfo.description)}>
                                <EditOutlined /><span>Chỉnh sửa</span>
                            </div>
                        </div>
                        <p>{restaurantInfo.description}</p>
                    </div>
                    <div className="event-category">
                        <div className="list-event-header">
                            <h3>Loại sự kiện cung cấp</h3>
                            <div className="edit" onClick={() => addEvent()}>
                                <EditOutlined /><span>Chỉnh sửa</span>
                            </div>
                        </div>
                        <div className="event-category-list">
                            {restaurantInfo.providedEvent.map(e => {
                                return <p>
                                    {iconMapping[e]} {typeMapping[e]}
                                </p>
                            })}
                        </div>
                    </div>
                    <div className="list-room">
                        <div className="list-room-header">
                            <h3>Các phòng sảnh</h3>
                            <div className="edit" onClick={() => addVenue()}>
                                <EditOutlined /><span>Thêm phòng sảnh</span>
                            </div>
                        </div>
                        <div className="rooms">
                            {restaurantInfo.venueList&&restaurantInfo.venueList.map(venue=>{
                                return (
                                    <div className="item item-mypage" onClick={() => showVenue(venue)}>
                                        <div className="image">
                                            <img src={venue.image||imgdetail1} alt=""/>
                                        </div>
                                        <div className="info">
                                            <h5>{venue.name}</h5>
                                            <p>Số người: {venue.numberTable.min} - {venue.numberTable.max}</p>
                                            <p>Loại sự kiện: {venue.type.map(e => typeMapping[e]).join(',')}</p>
                                            <p>Tiện ích: {venue.properties.map(e => <Tag color="orange">{e}</Tag>)}</p>
                                        </div>
                                        <div className="item__checkbox" onClick={e => e.stopPropagation()}>
                                            <Checkbox onChange={(e) => onChangeCheckVenue(e, venue._id)} checked={listVenueCheckbox.includes(venue._id)}></Checkbox>
                                        </div>
                                    </div>)
                            })}
                        </div>
                    </div>
                </div>
                <div className="wrap-detail__info-right">
                    <div className="side-address">
                        <h1 className="name-res">{restaurantInfo.name}</h1>
                        <div>
                            <p className="address">
                                <EnvironmentOutlined /> {`${restaurantInfo.address}`}
                            </p>
                        </div>
                        <div className="capa-price">
                            <div className="capa-price__block capa">
                                <p>Sức chứa tối đa: {restaurantInfo.capacity} Người</p>
                            </div>
                            <div className="capa-price__block price">
                                <p>Khoảng giá: {'1,000,000 VND'} - {'3,000,000 VND'} / người</p>
                            </div>
                        </div>

                    </div>
                    <div className="side-service">
                        <div className="service-header">
                            <h3>Gợi ý sản phẩm & dịch vụ</h3>
                        </div>
                        <p>Các loại sản phẩm & dịch vụ khác cho sự kiện của bạn</p>
                        <div className="list-services">
                            {listService.map((el, idx) => {
                                return <div className="item" onClick={() => showService(el)}>
                                            <img src={el.images[0]} alt=""/>
                                            <h4>{el.name}</h4>
                                            <p>Giá sản phẩm</p>
                                            <p className="price">{el.price.toLocaleString('it-IT', {style : 'currency', currency : 'VND'})}</p>
                                        </div>
                                })
                            }
                            <div></div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="list-menu">
                <div className="list-menu-header">
                    <h3>Danh sách món ăn</h3>
                    <div className="edit" onClick={() => addMenu()}>
                        <EditOutlined /><span>Thêm menu</span>
                    </div>
                </div>
                <div className="menus">
                    {restaurantInfo.venueList && restaurantInfo.venueList.map((venue, idx) => {
                        return listVenueCheckbox.includes(venue._id) && venue.menus.map(el => {
                            return <div className="item">
                                        <div className="image">
                                            <img src={el.image||"https://www.sassyhongkong.com/wp-content/uploads/2019/05/vegan-restaurants-veggie-veg-hk-kowloon.jpg"} alt=""/>
                                        </div>
                                        <div className="info">
                                            <h4>{el.name}</h4>
                                            <p>Số người: {el.numberPeople}</p>
                                            <p className="m-0">Giá sản phẩm</p>
                                            <p>{el.price.toLocaleString('it-IT', {style : 'currency', currency : 'VND'})}</p>
                                            <Button className="btn-primary" onClick={() => showModalMenu(el)}>Xem chi tiết</Button>
                                        </div>
                                    </div>
                            })

                        })
                    }
                    
                </div>
            </div>
            {/* <div className="list-drink">
                <h3>Danh sách đồ uống</h3>
                <div className="drinks">
                    {restaurantInfo.drinks && restaurantInfo.drinks.map((el, idx) => {
                        return <div className="item" key={idx}>
                                    <div className="image">
                                        <img src={el.image||"https://www.sassyhongkong.com/wp-content/uploads/2019/05/vegan-restaurants-veggie-veg-hk-kowloon.jpg"} alt=""/>
                                    </div>
                                    <div className="info">
                                        <h4>{el.name}</h4>
                                        <p className="m-0">Giá sản phẩm</p>
                                        <p>{el.price.toLocaleString('it-IT', {style : 'currency', currency : 'VND'})} đ</p>
                                        <Button className="btn-primary" onClick={() => showModalDrink(el)}>Xem chi tiết</Button>
                                    </div>
                                </div>
                        })
                    }
                    
                </div>

            </div> */}
            <div className="wrap-map">
                <h3>THÔNG TIN VỊ TRÍ</h3>
                <Mapbox coordinates={get(restaurantInfo , "location.coordinates")}/>
            </div>
        </div>
    ) 
};
export default PageDetail;
