import React, { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom'
import ApiRestaurant from '../../services/restaurant-service'
import ApiUser from '../../services/user-service'
import { CheckOutlined, EditOutlined, DeleteOutlined } from '@ant-design/icons'
import { Table } from 'antd'
import ModalPopup from '../../components/Modal'
import { Pie } from '@ant-design/charts';
import './ListUser.scss'
const getRole = (info) => {
  if(info.isSystemAdmin) return "sys_admin"
  if(!info.isSystemAdmin && info.restAdmin.length > 0) return "rest_admin"
  return "user"
}
const ListUser = () => {
    const [listUser, getListUser] = useState([])
    const data = [
      {
        type: 'User',
        value: listUser.filter(u => getRole(u) === "user").length,
      },
      {
        type: 'Rest_admin',
        value: listUser.filter(u => getRole(u) === "rest_admin").length,
      },
      {
        type: 'Sys_admin',
        value: listUser.filter(u => getRole(u) === "sys_admin").length,
      }
    ];
    const config = {
      width: 200,
      appendPadding: 10,
      data: data,
      angleField: 'value',
      colorField: 'type',
      radius: 0.35,
      label: {
        type: 'spider',
        labelHeight: 28,
        content: '{name}\n{value} ({percentage})',
      },
      interactions: [{ type: 'element-selected' }, { type: 'element-active' }],
      style: {
        width: 500
      }
    };
    const columns = [
        {
          title: 'ID',
          dataIndex: '_id',
          render: (_, record) => _.slice(0,8)
        },
        {
          title: 'Tên',
          dataIndex: 'name',
          sorter: (a, b) => a.name < b.name ? 1 : a.name > b.name ? -1 : 0,
          sortDirections: ['ascend', 'descend'],
        },
        {
          title: 'Email',
          dataIndex: 'email',
        },
        {
          title: 'Phone',
          dataIndex: 'phone',
        },
        {
          title: 'ID Restaurant',
          dataIndex: 'rest_id',
          render: (_, record) => _.slice(0,8)
        },
        {
          title: 'Role',
          dataIndex: 'role',
          filters: [
            {
              text: 'User',
              value: 'user',
            },
            {
              text: 'Rest_admin',
              value: 'rest_admin',
            },
            {
              text: 'Sys_admin',
              value: 'sys_admin',
            },
          ],
          onFilter: (value, record) => getRole(record).indexOf(value) === 0
        },
        {
          title: 'Action',
          dataIndex: 'action',
          render: (_, record) => {
              return <div className="action" style={{display:'flex'}}>
                  <div style={{ marginRight: '5px', cursor: 'pointer'}}><EditOutlined /></div>
                  <div onClick={() => deleteUser(record._id)} style={{ marginRight: '5px', cursor: 'pointer'}}><DeleteOutlined /></div>
              </div>
          }
        },
    ];

    const deleteUser = async (id) => {
      return  ModalPopup({
        type: 'confirm',
        content: () => <p>Are you sure?</p>,
        onOk: async () => {
          await ApiUser.deleteUser(id)
          initListUser()
        }
      })
    }
    const initListUser = async () => {
      const res = await ApiUser.getListUser()
      if(res.ok) {
        let dataTable = res.data.map(e => {
          return {
            ...e,
            _id: e._id,
            rest_id: e.restAdmin.length > 0 ? e.restAdmin[0] : '',
            role: getRole(e)
          }
        })
        getListUser(dataTable)
      }
    }
    useEffect(() => {
      initListUser()
    }, [])
    return (
        <div className="statis-page">
          <div className="statis-page-header">
            <h3>Danh sách người dùng</h3>
            <Pie {...config} />

          </div>
            <Table 
                columns={columns} 
                dataSource={listUser} 
                rowClassName="request-page-row" 
                onRow={(record, rowIndex) => {
                    return {
                      onClick: event => { 
                          // history.push(`/page-detail/${record.key}`)
                       },
                    };
                }}
            />
        </div>
    );
}
export default ListUser