import React, { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom'
import ApiRestaurant from '../../services/restaurant-service'
import ApiUser from '../../services/user-service'
import { UserOutlined, MailOutlined } from '@ant-design/icons'
import { message, Table } from 'antd'
import ModalPopup from '../../components/Modal'
import queryString from "query-string"

const UserInfo = () => {
    const history = useHistory()
    const verify = async (token) => {
        const res = await ApiUser.verify(token)
        if(res.ok) {
            history.push("/signupuser")
            message.success("Verify success!")
        }
    }
    useEffect(() => {
        const q = queryString.parse(history.location.search)
        verify(q.token)
    },[])
    return (
        <div className="verify-wrap">
            
        </div>
    );
}
export default UserInfo