import React, { useState } from 'react';
import logo from '../logo.svg';
import ModalPopup from '../components/Modal'
import { Select, InputNumber,Button, Modal } from 'antd'
import { useHistory } from 'react-router-dom';
import './SearchPage.scss'

const listEvent = [
    { value: 'tieccuoi', label: 'Tiệc cưới'},
    { value: 'hoinghi', label: 'Hội nghị/hội thảo'},
    { value: 'lienhoan', label: 'Liên hoan/Tiệc'},
    { value: 'sinhnhat', label: 'Sinh nhật'},
    { label: 'Họp nhóm', value: "hopnhom" },
    { label: 'Workshop', value: "workshop" },
]
const SearchPage = () => {
    const history = useHistory()

    const AutoQuote = () => {
        const [event, setEvent] = useState('select event')
        const [numberPeople, setNumberPeople] = useState(1)
        const [totalPrice, setTotalPrice] = useState(0)
        const onChangeEvent = (e) => {
            setEvent(e)
        }
        const onChangePeople = (e) => {
            setNumberPeople(e)
        }
        const onChangePrice = (e) => {
            setTotalPrice(e)
        }
        const onClickSearch = () => {
            // history.push({
            //     pathname: '/quote',
            //     search: `?event=${event}&people=${numberPeople}&price=${totalPrice}`,
            //   });
            history.push(`/quote?event=${event}&people=${numberPeople}&price=${totalPrice}`)
            Modal.destroyAll()
        }
        return <div className="auto-quote">
            <div className="block-1 providedEvent">
                <h3>* Loại hình sự kiện</h3>
                <Select style={{ width: '100%' }} value={event} onChange={onChangeEvent}>
                    {listEvent.map(event => (
                        <Select.Option value={event.value} key={event.value}>{event.label}</Select.Option>
                    ))}
                </Select>
            </div>
            <div className="block-2">
                <div className="block-2__item capacity">
                    <h3>Số người</h3>
                    <InputNumber value={numberPeople} onChange={onChangePeople}></InputNumber>
                </div>
                <div className="block-2__item totalPrice">
                    <h3>Chi phí dự tính</h3>
                    <div style={{ display: 'flex', alignItems: 'center'}}>
                    <InputNumber
                        value={totalPrice}
                        onChange={onChangePrice}
                    ></InputNumber>VND
                    </div>
                </div>
            </div>
            <div className="btn-group" style={{ marginTop: '20px', display: 'flex', justifyContent: 'flex-end'}}>
                <Button onClick={() => Modal.destroyAll()}>Cancel</Button>
                <Button type="primary" style={{ marginLeft: '10px'}} onClick={onClickSearch}>Tìm kiếm</Button>
            </div>
        </div>
    }
    const modalAuto = () => {
        return  ModalPopup({
            // type: "confirm",
            title: "Báo giá tự động",
            okText: "Tìm kiếm",
            content: () => <AutoQuote />,
            propsContent: {}
        })
    }
    return (
        <div className="header-search">
            <div className="header-top">
                <img src={ logo } alt="logo"></img>
            </div>
            <div className="home-search">
                <h2>MIỄN PHÍ TÌM KIẾM ĐỊA ĐIỂM - DỊCH VỤ TỔ CHỨC SỰ KIỆN</h2>
                <p>Tiết kiệm chi phí và thời gian với 1.000+ đối tác uy tín tại Việt Nam</p>
                {/* <Search /> */}
                <div className="home-search-block">
                    <button type="button"  className="btn btn-warning btn-lg" onClick={() => history.push('/home')}>Danh sách nhà hàng</button>
                </div>
                <div className="home-search-block">
                    <button type="button"  className="btn btn-warning btn-lg" onClick={modalAuto}>Báo giá tự động</button>
                </div>
            </div>
        </div>
    );
}

export default SearchPage