import React, {useState, useEffect} from 'react';
import Restaurant from '../components/restaurant';
import FilterHome from '../components/filterHome';
import ApiRestaurant from '../services/restaurant-service'
import {DownOutlined} from "@ant-design/icons"
import './homepage.scss';
import Autocomplete from "react-google-autocomplete";
const HomePage = (props) => {

  const [filterRestaurant, setFilterRestaurant] = useState({})
  const [hasNextPage, setHasNextPage] = useState(false)

  const [listRestaurant, setListRestaurant] = useState([])
  const [cursor , setCursor] = useState(false)
  const loadMore = async () => {
    const response = await ApiRestaurant.getListRestaurant({...filterRestaurant, cursor})
    if(response.ok) {
      if(response.data.hasNextPage){
        const cursor_res = response.data.cursor
        setHasNextPage(true)
        setCursor(cursor_res)
        // setFilterRestaurant({...filterRestaurant })
      } else {
        setHasNextPage(false)
      }
      if(response.data.restaurants) setListRestaurant([...listRestaurant,...response.data.restaurants])
      else setListRestaurant([])
    }
  }
  const initListRestaurant = async () => {
    const response = await ApiRestaurant.getListRestaurant(filterRestaurant)
    if(response.ok) {
      if(response.data.hasNextPage){
        const cursor_res = response.data.cursor
        setCursor(cursor_res)
        setHasNextPage(true)
      } else {
        setHasNextPage(false)
      }
      if(response.data.restaurants) setListRestaurant(response.data.restaurants)
      else setListRestaurant([])
    }
  }

  useEffect(() => {
    initListRestaurant()
  },[])
  useEffect(() => {
    console.log(filterRestaurant)
    initListRestaurant()
  },[filterRestaurant])


  return(
      <div className="main">
          <div className="container">
              <div className="row">
                  {/* <div className="filter-home"> */}
                    <FilterHome 
                        setFilterRestaurant = { setFilterRestaurant }
                        filterRestaurant = { filterRestaurant }
                    />
                  {/* </div> */}

                  <div className="list-restaurant col-md-9 border-top">
                      <div className="title">
                          <h3>Danh sách nhà hàng tại Hồ Chí Minh</h3>
                          <hr/>
                      </div>
                      <div className="items">
                          {listRestaurant.map((infores) =>{
                              return <Restaurant info={infores} />
                          })}
                          <div className="w-33 mb-20"></div>
                      </div>
                      {hasNextPage && <div style={{justifyContent:"center", alignItem: "center", display: "flex"}}>
                        <div className="load-more-btn" onClick={loadMore}>
                          <DownOutlined style={{marginRight:"10px"}}/>
                          <div>Load more</div>
                        </div>
                      </div>}
                  </div>
              </div>
          </div>
      </div>
  )
}
export default HomePage;