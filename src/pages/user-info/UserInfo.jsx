import React, { useState,useContext, useEffect } from 'react';
import ApiUser from '../../services/user-service'
import { UserOutlined, MailOutlined, PhoneOutlined } from '@ant-design/icons'
import UserContext from '../../context/info-context'
import { Input, Button } from 'antd'
import './UserInfo.scss'
// const { InputNumber } = Input
const UserInfo = () => {

    const { userInfo, getUserInfo } = useContext(UserContext)
    const [name, setName] = useState('')
    const [phone, setPhone] = useState('')
    console.log(userInfo)
    const onChangeName = (e) => {
        console.log(e)
        setName(e.target.value)
    }
    const onChangePhone = (e) => {
        setPhone(e.target.value)
    }

    const updateInfo = async () => {
        let formData = {
            name: name,
            phoneNumber: phone
        }
        const res = await ApiUser.updateInfo(formData)
        if(res.ok) {
            getUserInfo()
        }

    }
    useEffect(() => {
        if(userInfo && userInfo.info) {
            setName( userInfo.info.name)
            setPhone( userInfo.info.phoneNumber)
        }
    }, [userInfo])
    return (
        <div className="userinfo-wrap">
        <div className="userinfo-page">
            <h3>Thông tin tài khoản</h3>
            <div className="block name">
                <div className="content">
                    <span className="title"><UserOutlined style={{ color: "#002f79"}}/> Tên</span>
                    <Input value={name} onChange={onChangeName}></Input>
                </div>
            </div>
            <div className="block mail">
                <div className="content">
                    <span className="title"><MailOutlined style={{ color: "#002f79"}} /> Email</span>
                    <Input disabled  defaultValue={userInfo.info && userInfo.info.email}></Input>
                </div>
            </div>
            <div className="block mail">
                <div className="content">
                    <span className="title"><PhoneOutlined style={{ color: "#002f79"}} /> Phone</span>
                    <Input value={phone} onChange={onChangePhone}></Input>
                </div>
            </div>

            <div className="group-btn">
                <Button type="primary" onClick={updateInfo}>Cập nhật</Button>
            </div>
        </div></div>
    );
}
export default UserInfo