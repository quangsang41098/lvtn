import React, { useState, useEffect, useContext } from 'react';
import logo from '../logo.svg';
import ApiRestaurant from '../services/restaurant-service'
import { useHistory } from 'react-router-dom'
import { Form, Input, Checkbox, message, Select } from "antd"
import { LIST_DISTRICT, LIST_WARD } from '../constants/const-address'
import Autocomplete from "../components/Autocomplete";
import UserContext from '../context/info-context'
const GOOGLE_MAPS_API_KEY = process.env.REACT_APP_GG_MAP_API;
const loadGoogleMapScript = (callback) => {
    if (
      typeof window.google === "object" &&
      typeof window.google.maps === "object"
    ) {
      callback();
    } else {
      const googleMapScript = document.createElement("script");
      googleMapScript.src = `https://maps.googleapis.com/maps/api/js?key=${GOOGLE_MAPS_API_KEY}&libraries=places`;
      window.document.body.appendChild(googleMapScript);
      googleMapScript.addEventListener("load", callback);
    }
};
const optionsCheckbox = [
    { label: 'Tiệc cưới', value: "tieccuoi" },
    { label: 'Sinh nhật', value: "sinhnhat" },
    { label: 'Hội thảo/Hội nghị', value: "hoinghi" },
    { label: 'Liên hoan/Tiệc', value: "lienhoan" },
    { label: 'Họp nhóm', value: "hopnhom" },
    { label: 'Workshop', value: "workshop" },
];
const SignUpRestaurantPage = () => {
    const history = useHistory()
    
    const { userInfo, getUserInfo } = useContext(UserContext)
    const [loadMap, setLoadMap] = useState(false);
    const [form] = Form.useForm();
    const [providedEvent, setProvidedEvent] = useState([])
    const [addrDetail, setAddrDetail] = useState('')
    const [coordinates, setCoordinates] = useState([])
    const [placeId, setPlaceId] = useState(null)

    const onFinish = async (data) =>{
        const response = await ApiRestaurant.registerRestaurant({
            ...data, 
            providedEvent, address: addrDetail, location: coordinates, placeId,
            email: userInfo.info.email
        })
        message.success("Đăng kí thành công")
        getUserInfo()
        history.push('/mypage')
    }
    const onChangeCheckbox = (value) => {
        setProvidedEvent(value)
    }
    
    useEffect(() => {
        loadGoogleMapScript(() => {
          setLoadMap(true);
        });
      }, []);
    return (
        <div className="main mb-2">
            <div className="container">
                <div className="row">
                    <div className="col-md-9">
                        <div className="title">
                            <h3>Đăng ký nhà hàng vào hệ thống</h3>
                            <hr />
                        </div>
                        <div className="container">
                            <strong>Thông tin liên hệ</strong>
                            <hr/>
                            <Form onFinish={onFinish} form={form}>
                                <label for="NameRestaurant">Tên nhà hàng</label>
                                <div>
                                <Form.Item
                                    name="name"
                                    rules={[{ required: true, message: 'Vui lòng nhập tên nhà hàng!' }]}
                                >
                                    <Input placeholder="Nhập địa chỉ"  />
                                    
                                </Form.Item>
                                </div>
                                <label for="NameRestaurant">Địa chỉ</label>
                                <Form.Item
                                    name="address_detail"
                                    // rules={[{ required: true, message: 'Vui lòng nhập địa chỉ!' }]}
                                >
                                    {!loadMap ? 
                                        <div>Loading...</div> : 
                                        <Autocomplete 
                                            setAddrDetail = {setAddrDetail}
                                            setCoordinates = {setCoordinates}
                                            setPlaceId={setPlaceId}
                                            name="name"
                                        />
                                    }
                                </Form.Item>

                                <label for="NameRestaurant">Email</label>
                                <Form.Item
                                    name="email"
                                    // rules={[{ required: true, message: 'Vui lòng nhập email!' }]}
                                >
                                    <Input placeholder="Email" defaultValue={userInfo.info && userInfo.info.email} disabled/>
                                </Form.Item>
                                <div className="form-row">
                                    <div className="phone col-md-6">
                                        <label for="NameRestaurant">Số điện thoại</label>
                                        <Form.Item
                                            name="hotline"
                                            rules={[{ required: true, message: 'Vui lòng nhập số điện thoại!' }]}
                                        >
                                            <Input placeholder="Số điện thoại"/>
                                        </Form.Item>
                                    </div>
                                    <div className="capacity col-md-6">
                                        <label for="NameRestaurant">Sức chứa</label>
                                        <Form.Item
                                            name="capacity"
                                            rules={[{ required: true, message: 'Vui lòng nhập sức chứa!' }]}
                                        >
                                            <Input placeholder="Sức chứa"/>
                                        </Form.Item>
                                    </div>
                                </div>
                                <label for="NameRestaurant">Mô tả</label>
                                <Form.Item
                                    name="description"
                                >
                                    <Input.TextArea placeholder="Mô tả"/>
                                </Form.Item>
                                <label for="events">Các sự kiện</label><br />
                                <Form.Item
                                    name="providedEvent"
                                >
                                    <Checkbox.Group options={optionsCheckbox}  name="providedEvent" onChange={onChangeCheckbox} /><br/>
                                </Form.Item>
                                <button className="btn btn-primary" type="submit">Đăng kí</button>
                            </Form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}
export default SignUpRestaurantPage;