import React, { useEffect, useContext } from "react";
import { Checkbox, InputNumber, Button, DatePicker, Select, message, Typography } from "antd";
import { MinusOutlined, PlusOutlined, DeleteOutlined } from "@ant-design/icons";
import "./CartPage.scss";
import imgdetail1 from "../../images/detail1.jpg";
import { loadStateFromLocal, editCartItemToLocal, removeCartItemToLocal } from "../../plugins/localstorage";
import { find } from "lodash";
import { useHistory } from "react-router-dom";
import UserContext from '../../context/info-context'
import queryString from "query-string"
import moment from "moment";

const {Text} = Typography
const CartPage = () => {
    const { userInfo, setSelectedCart } = useContext(UserContext)
	const [checkedList, setCheckedList] = React.useState([]);
	const [indeterminate, setIndeterminate] = React.useState(false);
	const [checkAll, setCheckAll] = React.useState(true);
	const [carts, setCarts] = React.useState([]);
	const [currentCart, setCurrentCart] = React.useState();
	const [tempPrice, setTempPrice] = React.useState(0);
	const [date, setDate] = React.useState(null);
	const [time, setTime] = React.useState(1);
	const history = useHistory()
	const onChange = e => {
		let checkListUpdate = [];
		if(e.target.checked) {
			checkListUpdate = [...checkedList, e.target.value];
			setCheckedList(checkListUpdate);
		} else {
			const index = checkedList.indexOf(e.target.value);
			if (index > -1) {
				checkListUpdate = [...checkedList];
				checkListUpdate.splice(index, 1);
				setCheckedList(checkListUpdate);
			}
		}
		setIndeterminate(!!checkListUpdate.length && checkListUpdate.length < currentCart.list_item.length);
		setCheckAll(checkListUpdate.length === currentCart.list_item.length);
	};

	const onCheckAllChange = (e, cart) => {
		setCheckedList(e.target.checked ? cart.list_item.map(e => e.item_name) : []);
		setIndeterminate(!indeterminate);
		setCheckAll(e.target.checked);
		setCurrentCart(cart);
		if(cart && cart.selectedDate) setDate(cart.selectedDate)
	};

	const onChangeQuantity = (e, item) => {
		if(e !== item.item_quantity && e >= 0 && e <= 1000) {
			let cart = editCartItemToLocal({
				restaurant_id: currentCart.restaurant_id,
				item: {...item, item_quantity: e }
			});
			setCarts(cart);
			setCurrentCart(find(cart, { restaurant_id: currentCart.restaurant_id }));
		}
	};

	const removeItem = (item) => {
		let cart = removeCartItemToLocal({restaurant_id: currentCart.restaurant_id, item});
		setCarts(cart);
		let curCart = find(cart, { restaurant_id: currentCart.restaurant_id }) || cart[0]
		setCurrentCart(curCart);
	};

	const initCart = () => {
		let cart = loadStateFromLocal();
		if(cart && cart.length > 0) {
			setCarts(cart);
			setCurrentCart(cart[0]);
			setCheckedList(cart[0].list_item.map(e => e.item_name));
			if(cart[0] && cart[0].selectedDate) setDate(cart[0].selectedDate)
		}
	};

	const deposit = async () => {
		if(userInfo.role === "guest") return message.error("Bạn cần đăng nhập để sử dụng chức năng này!")
		if(!date) return message.error("Bạn cần nhập thời gian tổ chức!")
		let list_item = currentCart.list_item
		let menu = list_item.filter(e => e.item_type === "menu").map(e => {return {menuId: e.item_id, quantity: e.item_quantity}})
		let services = list_item.filter(e => e.item_type === "service").map(e => e.item_id)
		setSelectedCart({
			amount: parseInt((tempPrice * 1.1 * 0.2).toFixed(0)),
			receipt_email: userInfo.info.email, 
			time: {
				date: date,
				session: time
			},
			userId: userInfo.info._id,
			menu,
			services,
			venue: currentCart.venueId,
			rest_id: currentCart.restaurant_id,
			total: tempPrice
		})
		history.push("/payment")
	}
	const onChangeDate = (value) => {
		setDate(value.unix())
	}
	const onChangeTime = (value) => {
		setTime(value)
	}
	useEffect(() => {
		initCart();
	},[]);
	useEffect(() => {
        const q = queryString.parse(history.location.search)
        if(q.rest_id && carts && carts.length > 0) {
			let cart = carts.find(e => e.restaurant_id === q.rest_id)
			console.log(cart)
			setCheckedList(true ? cart.list_item.map(e => e.item_name) : []);
			setIndeterminate(!indeterminate);
			setCheckAll(true);
			setCurrentCart(cart);
			if(cart && cart.selectedDate) setDate(cart.selectedDate)
		}
	},[carts]);

	useEffect(() => {
		let listItemCheck = currentCart && currentCart.list_item.filter(item => checkedList.includes(item.item_name));
		if(listItemCheck && listItemCheck.length > 0) {
			let price = 0
			for (let i of listItemCheck) {
				price += i.item_price * i.item_quantity;
			}
			setTempPrice(price);
		} else setTempPrice(0);
	},[currentCart, checkAll, indeterminate, checkedList, carts]);
	return (
		<div className="wrap-cartpage">
			<div className="title">
				<h3>Giỏ Hàng</h3>
			</div>
			{carts && carts.length === 0 && 
				<p>Hiện tại giỏ hàng còn trống</p>
			}
			<div className="container-cart">
				<div className="container-cart-left">
					<div className="list-restaurant-cart">
						{carts && carts.length > 0 && carts.map(cart => {
							return <div className="restaurant-cart">
								<div className="cart-header">
									<Checkbox
										indeterminate={currentCart.restaurant_id !== cart.restaurant_id ? false : indeterminate}
										onChange={(e) => onCheckAllChange(e, cart)}
										checked={currentCart.restaurant_id !== cart.restaurant_id ? false : checkAll}
									>
										{cart.restaurant_name}
									</Checkbox>
									<span className="detail" onClick={() => history.push(`/page-detail/${cart.restaurant_id}`)}>Go to detail</span>
								</div>
								<div className={`cart-body ${currentCart.restaurant_id !== cart.restaurant_id ? "disable" : ""}`}>
									<div className="list-items">
										{cart.list_item.map(item => {
											return <div className="item">
												<div className="check-box">
													<Checkbox
														value={item.item_name}
														onChange={onChange}
														checked={(currentCart.restaurant_id === cart.restaurant_id && checkedList.includes(item.item_name)) ? true : false}>
													</Checkbox>
												</div>
												<div className="info">
													<div className="image">
														<img src={imgdetail1} alt=""/>
													</div>
													<div className="content">
														<div className="name">
															<p>{item.item_name}</p>
														</div>
													</div>
												</div>
												<div className="price">
													<p>{item.item_price}</p>
												</div>
												<div className="quantity">
													<Button type="primary" onClick={() => onChangeQuantity(item.item_quantity - 1, item)} disabled={item.item_type === "service"}><MinusOutlined /></Button>
													<InputNumber min={0} max={1000} value={item.item_quantity} onChange={(e) => onChangeQuantity(e, item)} disabled={item.item_type === "service"} />
													<Button type="primary" onClick={() => onChangeQuantity(item.item_quantity + 1, item)} disabled={item.item_type === "service"}><PlusOutlined /></Button>
												</div>
												<div className="remove-item" onClick={() => removeItem(item)}>
													<DeleteOutlined />
												</div>
											</div>;})}
									</div>
								</div>
							</div>;})}
					</div>
				</div>
				<div className="container-cart-right">
					<div className="cart-info">
						<h4>Thông tin đơn hàng</h4>
						<div className="price">
							<p className="block-left">Tạm tính</p>
							<p>{tempPrice.toLocaleString("it-IT", {style : "currency", currency : "VND"})}</p>
						</div>
						<div className="vat">
							<p className="block-left">Thuế VAT (10%)</p>
							<p>{(tempPrice * 0.1).toLocaleString("it-IT", {style : "currency", currency : "VND"})}</p>
						</div>
						<div className="total-price">
							<p className="block-left">Tổng cộng</p>
							<p>{(tempPrice + tempPrice * 0.1).toLocaleString("it-IT", {style : "currency", currency : "VND"})}</p>
						</div>
						<div className="total-price">
							<p className="block-left">Đặt cọc (20%)</p>
							<p>{(tempPrice * 1.1 * 0.2).toLocaleString("it-IT", {style : "currency", currency : "VND"})}</p>
						</div>
						
						<div className="time">
							<p className="block-left">Thời gian</p>
							<div className="d-flex">
								<DatePicker 
									size="default" 
									onChange={onChangeDate}    
									value={(date && moment.unix(date)) || currentCart && currentCart.selectedDate && moment.unix(currentCart.selectedDate)}
								/>
								{/* <Select 
									defaultValue="1" 
									onChange={onChangeTime}
								>
									<Option value="1">Sáng</Option>
									<Option value="2">Tối</Option>
								</Select> */}
							</div>
						</div>
						<div className="block-order-btn">
							<Button className="btn-order" onClick={()=> deposit()}>Đặt cọc</Button>
						</div>
					</div>
				</div>
			</div>

		</div>
	);
};

export default CartPage;