import React, { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom'
import ApiRestaurant from '../../services/restaurant-service'
import ApiUser from '../../services/user-service'
import { CheckOutlined, MailOutlined } from '@ant-design/icons'
import { Table, Typography, Collapse } from 'antd'
import ModalPopup from '../../components/Modal'
const { Panel }  = Collapse 
const { Text } = Typography
const UserOrder = () => {
    const columns = [
        {
          title: 'Mã đơn hàng',
          dataIndex: 'code',
          render: (_, record) => _.slice(0,15),
          sorter: (a, b) => a.code < b.code ? 1 : a.code > b.code ? -1 : 0,
          sortDirections: ['ascend', 'descend'],
        },
        {
          title: 'Tên nhà hàng',
          dataIndex: 'rest_name',
        },
        {
          title: 'Menu',
          dataIndex: 'menu',
          render: (_, record) => <div onClick={() => toggleMenuDetail(_, record.services)} style={{ cursor: 'pointer'}}><Text type="success">Chi tiết</Text></div>
        },
        {
          title: 'Phòng sảnh',
          dataIndex: 'venue',
        },
        {
          title: 'Giá tiền (VNĐ)',
          dataIndex: 'price',
          render: (_, record) => <span>{record.status === "deposit" ? _.toLocaleString("it-IT") : record.totalPrice.toLocaleString("it-IT")}</span>,
          sorter: (a, b) => a.price < b.price ? 1 : a.price > b.price ? -1 : 0,
          sortDirections: ['ascend', 'descend'],
        },
        {
          title: 'Ngày đặt',
          dataIndex: 'created_at',
          render: (_, record) => <span>{_.date}</span>,
          sorter: (a, b) => a.created_at.etimestamp < b.created_at.etimestamp ? 1 : a.created_at.etimestamp > b.created_at.etimestamp ? -1 : 0,
          sortDirections: ['ascend', 'descend'],
        },
        {
          title: 'Ngày tổ chức',
          dataIndex: 'date',
          render: (_, record) => <span>{_.session === 2 ? "Tối" : "Sáng"}, {_.date}</span>,
          sorter: (a, b) => a.date.timestamp < b.date.timestamp ? 1 : a.date.timestamp > b.date.timestamp ? -1 : 0,
          sortDirections: ['ascend', 'descend'],
        },
        {
          title: 'Status',
          dataIndex: 'status',
        },
    ];
    const [listOrder, setListOrder] = useState([])
    const toggleMenuDetail = (menu, services) => {
        return  ModalPopup({
            content: () => MenuDetail(menu, services),
            className: "ant-modal-menu-detail"
        })
    }
    const MenuDetail = (menu, services) => {
        return <div className="modal-menu-detail">
            <h3>Chi tiết menu</h3>
            <div className="list-item">
                <div className="item item1">
                    <div className="block-1">
                        <div className="block name">
                            <span>Tên sản phẩm</span>
                        </div>
                        <div className="block unit-price">
                            <span>Đơn giá</span>
                        </div>
                        <div className="block quantity">
                            <span>Số lượng</span>
                        </div>
                    </div>
                </div>
                
                {menu.map(m => { 
                    return <div className="item item2">
                    <div className="block-1">
                        <div className="block name">
                            <span>{m.name}</span>
                        </div>
                        <div className="block unit-price">
                            <span>{m.price}</span>
                        </div>
                        <div className="block quantity">
                            <span>{m.quantity}</span>
                        </div>
                    </div>
                    <div className="detail">
                        <Collapse defaultActiveKey={['1']}>
                            <Panel header="Menu chi tiết" key="1">
                                {m.foodList.map(e => {
                                    return <div>{e}</div>
                                })}
                            </Panel>
                        </Collapse>
                    </div>
                </div>})}
                {services.map(s => <div className="item item2">
						<div className="block-1">
							<div className="block name">
								<span>{s.name}</span>
							</div>
							<div className="block unit-price">
								<span>{s.price.toLocaleString("it-IT", {style : "currency", currency : "VND"})}</span>
							</div>
							<div className="block quantity">
								<span>{1}</span>
							</div>
						</div>
                </div>)}
            </div>
        </div>
    }
    const initOrder = async () => {
        const response = await ApiUser.getOrder()
        if(response.ok) {
            let result = response.data.result.map((el, idx) => {
                return {
                    code: el.orderId,
                    rest_name: el.restaurantName,
                    venue: el.venue,
                    date: el.time,
                    price: el.deposit,
                    totalPrice: el.totalPrice,
                    created_at: el.createdAt,
                    menu: el.menu,
                    services: el.services,
                    status: "Đã cọc"
                }
            })
            setListOrder(result)
        }

    }
    useEffect(() => {
        initOrder()
    }, [])
    return (
        <div className="statis-page">
            <h3>Đơn hàng đã đặt</h3>
            <Table 
                columns={columns} 
                dataSource={listOrder} 
                rowClassName="request-page-row" 
                onRow={(record, rowIndex) => {
                    return {
                      onClick: event => { 
                        //   history.push(`/page-detail/${record.key}`)
                       },
                    };
                }}
            />
        </div>
    );
}
export default UserOrder