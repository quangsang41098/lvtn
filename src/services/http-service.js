import axios from "axios";
import { get } from "lodash";

axios.defaults.baseURL = process.env.REACT_APP_API_URL || "http://localhost:4300/api";

// axios.defaults.baseURL = "http://localhost:4300/api";
const APIServices = {
	sendRequest: (
		method,
		url,
		headerParams,
		body,
		{ timeout = 60000 } = {},
		responseType = "json"
	) => {
		const headers = headerParams || { "Content-Type": "application/json" };
		const request = {
			url,
			method,
			timeout,
			headers,
			responseType,
		};
		if (!method.match(/get|head|delete/)) {
			request.data = body || {};
		}
		if (method === "delete" || method === "get") {
			// request.data = body || {};
			request.params = body || {};

		}
		return new Promise((resolve) => {
			axios
				.request(request)
				.then((res) => {
					if (res.status >= 200 && res.status < 300) {
						resolve(res);
					} else {
						resolve({ isError: true, ...res.data });
					}
				})
				.catch((err) => resolve({ isError: true, err }));
		});
	},

	sendRequestWithToken: (
		method,
		url,
		headerParams,
		body,
		{ timeout = 60000 } = {},
		responseType = "json"
	) => {
		// customize by backend api
		const headers = headerParams || {};
		const localStorageRedux = JSON.parse(localStorage.user_info || "{}");

		const token = get(localStorageRedux, "token", "");

		headers.authorization = `Bearer ${token}`;

		return APIServices.sendRequest(
			method,
			url,
			headers,
			body,
			{ timeout },
			responseType
		);
	}
};

export default APIServices;
