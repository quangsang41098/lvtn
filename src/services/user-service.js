
import APIServices from './http-service';


const loginUser = async ({ password, email }) => {
    try {
    const response = await APIServices.sendRequest(
        'post',
        '/auth/login',
        {},
        {
        email,
        password,
        }
    );
    if (response.isError) throw response.err;
    
    const data = JSON.stringify({auth: response.data.auth, token: response.data.token});
    localStorage.setItem('user_info', data);

    return { ok: true, data: response.data };
    } catch (err) {
    return { ok: false, data: err };
    }
};

const getUserInfo = async () => {
    try {
    const response = await APIServices.sendRequestWithToken(
        'get',
        '/auth/me',
        {},
    );
    if (response.isError) throw response.err;

    return { ok: true, data: response.data };
    } catch (err) {
    return { ok: false, data: err };
    }
};

const getListUser = async (type) => {
    let query = type ? `?type=${type}` : ''
    try {
    const response = await APIServices.sendRequestWithToken(
        'get',
        `/users${query}`,
        {},
    );
    if (response.isError) throw response.err;

    return { ok: true, data: response.data };
    } catch (err) {
    return { ok: false, data: err };
    }
};

const registerUser = async ({ name, password, email }) => {
    try {
        const response = await APIServices.sendRequest(
            'post',
            '/auth/register',
            {},
            {
                name,
                password,
                email
            }
        );
        if (response.isError) throw response.err;
        return { ok: true, data: response.data };
    } catch (err) {
        return { ok: false, data: err };
    }
};

const deleteUser = async (id) => {
    try {
        const response = await APIServices.sendRequest(
            'delete',
            `/users/${id}`,
            {}
        );
        if (response.isError) throw response.err;
        return { ok: true, data: response.data };
    } catch (err) {
        return { ok: false, data: err };
    }
};

const updateInfo = async (formData) => {
    try {
        const response = await APIServices.sendRequestWithToken(
            'post',
            `/users/update-info`,
            {},
            formData
        );
        if (response.isError) throw response.err;
        return { ok: true, data: response.data };
    } catch (err) {
        return { ok: false, data: err };
    }
};

const acceptRestaurant = async (restId) => {
    try {
        const response = await APIServices.sendRequestWithToken(
            'post',
            '/admin/rest/accept',
            {},
            {
                restId
            }
        );
        if (response.isError) throw response.err;
        return { ok: true, data: response.data };
    } catch (err) {
        return { ok: false, data: err };
    }
};

const verify = async (token) => {
    try {
        const response = await APIServices.sendRequest(
            'post',
            '/auth/verify-account',
            {},
            {
                token
            }
        );
        if (response.isError) throw response.err;
        return { ok: true, data: response.data };
    } catch (err) {
        return { ok: false, data: err };
    }
};
const getOrder = async () => {
    try {
        const response = await APIServices.sendRequestWithToken(
            'get',
            '/users/orders',
            {}
        );
        if (response.isError) throw response.err;
        return { ok: true, data: response.data };
    } catch (err) {
        return { ok: false, data: err };
    }
};
const getAdminOrder = async () => {
    try {
        const response = await APIServices.sendRequestWithToken(
            'get',
            '/restaurants/admin/orders',
            {}
        );
        if (response.isError) throw response.err;
        return { ok: true, data: response.data };
    } catch (err) {
        return { ok: false, data: err };
    }
};
const adminUpdateStatusOrder = async (orderId) => {
    try {
        const response = await APIServices.sendRequestWithToken(
            'post',
            '/restaurants/admin/update-status',
            {},
            {
                orderId
            }
        );
        if (response.isError) throw response.err;
        return { ok: true, data: response.data };
    } catch (err) {
        return { ok: false, data: err };
    }
};
const getRevenue = async (orderId) => {
    try {
        const response = await APIServices.sendRequestWithToken(
            'get',
            '/restaurants/admin/revenue',
            {},
        );
        if (response.isError) throw response.err;
        return { ok: true, data: response.data };
    } catch (err) {
        return { ok: false, data: err };
    }
};
export default {
    registerUser,
    loginUser,
    getUserInfo,
    acceptRestaurant,
    getListUser,
    deleteUser,
    verify,
    getOrder,
    getAdminOrder,
    adminUpdateStatusOrder,
    getRevenue,
    updateInfo
}