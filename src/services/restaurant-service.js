
import APIServices from "./http-service";

const registerRestaurant = async ({ name, location, address, listImage, providedEvent, capacity, description, menuList, email, hotline, services, reviews, venues }) => {
	const formData = {};
	if(name) formData.name = name;
	if(address) formData.address = address;
	if(location) formData.location = location;
	if(listImage) formData.listImage = listImage;
	if(providedEvent) formData.providedEvent = providedEvent;
	if(capacity) formData.capacity = parseInt(capacity);
	if(description) formData.description = description;
	if(menuList) formData.menuList = menuList;
	if(email) formData.email = email;
	if(hotline) formData.hotline = hotline;
	if(services) formData.services = services;
	if(reviews) formData.reviews = reviews;
	if(venues) formData.venues = venues;

	try {
		const response = await APIServices.sendRequestWithToken(
			"post",
			"/restaurants/create",
			{},
			formData
		);
		if (response.isError) throw response.err;
		return { ok: true, data: response.data };
	} catch (err) {
		return { ok: false, data: err };
	}
};

const getListRestaurant = async (data) => {
    let formData = {}
    if(data && data.typeAddr && data.typeAddr !== '0') formData.type = data.typeAddr
    if(data && data.capacity && data.capacity !== '0') formData.capacity = data.capacity
    if(data && data.rangePrice) formData.rangePrice = data.rangePrice
    if(data && data && data.searchKey) formData.searchKey = data.searchKey
    if(data && data.properties) formData.properties = data.properties
    if(data && data.cursor) formData.cursor = data.cursor

    try {
        const response = await APIServices.sendRequest(
            'get',
            `/restaurants`,
            {},
            formData
        );
        if (response.isError) throw response.err;
        return { ok: true, data: response.data };
    } catch (err) {
        return { ok: false, data: err };
    }
};


const getDetailRestaurant = async (id) => {
	try {
		const response = await APIServices.sendRequest(
			"get",
			`/restaurants/detail/${id}`,
			{}
		);
		if (response.isError) throw response.err;
		return { ok: true, data: response.data };
	} catch (err) {
		return { ok: false, data: err };
	}
};

const getRestaurantPending = async () => {
    try {
        const response = await APIServices.sendRequestWithToken(
            'get',
            `/restaurants/pending`,
            {}
        );
        if (response.isError) throw response.err;
        return { ok: true, data: response.data };
    } catch (err) {
        return { ok: false, data: err };
    }
};
const getRestaurantInquery = async (formData) => {
    try {
        const response = await APIServices.sendRequest(
            'post',
            `/restaurants/inquery`,
            {},
			formData
        );
        if (response.isError) throw response.err;
        return { ok: true, data: response.data };
    } catch (err) {
        return { ok: false, data: err };
    }
};

const updateRestaurant = async (formData, rest_id) => {
    try {
        const response = await APIServices.sendRequestWithToken(
            'post',
            `/restaurants/updateinfo/${rest_id}`,
            {},
			formData
        );
        if (response.isError) throw response.err;
        return { ok: true, data: response.data };
    } catch (err) {
        return { ok: false, data: err };
    }
};
const createMenu = async (formData, venue_id) => {
    try {
        const response = await APIServices.sendRequestWithToken(
            'post',
            `/restaurants/createmenu/${venue_id}`,
            {},
			formData
        );
        if (response.isError) throw response.err;
        return { ok: true, data: response.data };
    } catch (err) {
        return { ok: false, data: err };
    }
};
const createVenue = async (formData, rest_id) => {
    try {
        const response = await APIServices.sendRequestWithToken(
            'post',
            `/restaurants/createvenue/${rest_id}`,
            {},
			formData
        );
        if (response.isError) throw response.err;
        return { ok: true, data: response.data };
    } catch (err) {
        return { ok: false, data: err };
    }
};
const EditVenue = async (formData, venue_id) => {
    try {
        const response = await APIServices.sendRequestWithToken(
            'post',
            `/restaurants/updatevenue/${venue_id}`,
            {},
			formData
        );
        if (response.isError) throw response.err;
        return { ok: true, data: response.data };
    } catch (err) {
        return { ok: false, data: err };
    }
};
const EditMenu = async (formData, menuId) => {
    try {
        const response = await APIServices.sendRequestWithToken(
            'post',
            `/restaurants/updatemenu/${menuId}`,
            {},
			formData
        );
        if (response.isError) throw response.err;
        return { ok: true, data: response.data };
    } catch (err) {
        return { ok: false, data: err };
    }
};
const getService = async () => {
    try {
        const response = await APIServices.sendRequest(
            'get',
            `/restaurants/services`,
            {},
        );
        if (response.isError) throw response.err;
        return { ok: true, data: response.data };
    } catch (err) {
        return { ok: false, data: err };
    }
};
const getProperties = async () => {
    try {
        const response = await APIServices.sendRequest(
            'get',
            `/restaurants/venue/properties`,
            {},
        );
        if (response.isError) throw response.err;
        return { ok: true, data: response.data };
    } catch (err) {
        return { ok: false, data: err };
    }
};
const deleteVenue = async (venueId) => {
    try {
        const response = await APIServices.sendRequestWithToken(
            'post',
            `/restaurants/venue/remove`,
            {},
            { venueId }
        );
        if (response.isError) throw response.err;
        return { ok: true, data: response.data };
    } catch (err) {
        return { ok: false, data: err };
    }
};
const deleteMenu = async (menuId) => {
    try {
        const response = await APIServices.sendRequestWithToken(
            'post',
            `/restaurants/menu/remove`,
            {},
            { menuId }
        );
        if (response.isError) throw response.err;
        return { ok: true, data: response.data };
    } catch (err) {
        return { ok: false, data: err };
    }
};

export default {
    registerRestaurant,
    getListRestaurant,
    getDetailRestaurant,
    getRestaurantPending,
	getRestaurantInquery,
	updateRestaurant,
    createMenu,
    createVenue,
    getProperties,
    getService,
    EditVenue,
    EditMenu,
    deleteVenue,
    deleteMenu
}
