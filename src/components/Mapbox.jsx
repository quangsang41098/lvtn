import React, { useRef, useEffect, useState, useCallback } from 'react';
import '@mapbox/mapbox-gl-geocoder/dist/mapbox-gl-geocoder.css';
// eslint-disable-next-line
import MapboxWorker from 'worker-loader!mapbox-gl/dist/mapbox-gl-csp-worker';

import GoogleMapReact from 'google-map-react';
import './Mapbox.scss'
const Map = (props) => {
    const [coordinates, setCoordinate] = useState(props.coordinates || [59.955413, 30.337844])
    const renderMarkers = (map, maps)  => {
      let marker = new maps.Marker({
        position: {
          lat: coordinates[0],
          lng: coordinates[1]
        },
        map,
        title: 'Hello World!'
      });
      return marker
    }

    useEffect(() => {
      if(props.coordinates) setCoordinate(props.coordinates)
    }, [props.coordinates])
    return (
        <div style={{ height: '60vh', width: '100%' }}>
            <GoogleMapReact
              bootstrapURLKeys={{ key: process.env.REACT_APP_GG_MAP_API }}
              center={{
                  lat: coordinates[0],
                  lng: coordinates[1]
              }}
              defaultZoom={15}
              yesIWantToUseGoogleMapApiInternals={true}
              onGoogleApiLoaded={({map, maps}) => renderMarkers(map, maps)}
            >
            </GoogleMapReact>
        </div>
    );
};

export default Map;