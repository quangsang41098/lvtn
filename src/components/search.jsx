import React, { useState } from 'react';
import {
    Link
} from "react-router-dom";

const searchStyle = {
    display: 'flex',
    margin: '0 0'
}
const selectStyle = {
    // height: '100%'
}



const Search = ({search}) => {
    const [value, setValue] = useState({
        district: "0",
        categories: ''
    })

    const onHandleChangeDistrict = (event) => {
        setValue({...value, district: event.target.value});
    }
    const onHandleChangeCate = (event) => {
        setValue({...value, categories: event.target.value});
    }
    const onclick = () => {
        search(value);
    }
return (
    
    <div className="search justify-content-center" style = { searchStyle }>
        <select className="custom-select custom-select-lg" style = {selectStyle} >
            <option value="1">Hồ Chí Minh</option>
        </select>
        <select className="custom-select custom-select-lg" onChange = { onHandleChangeDistrict }>
            <option value={0}>Tất cả</option>
            <option value={1}>Quận 1</option>
            <option value={2}>Quận 2</option>
            <option value={3}>Quận 3</option>
            <option value={4}>Quận 4</option>
            <option value={5}>Quận 5</option>
            <option value={6}>Quận 6</option>
            <option value={7}>Quận 7</option>
            <option value={8}>Quận 8</option>
            <option value={9}>Quận 9</option>
        </select>
        <select className="custom-select custom-select-lg" onChange = { onHandleChangeCate }>
            <option value="">Tất cả</option>
            <option value="Wedding">Tiệc cưới</option>
            <option value="Birthday">Sinh nhật</option>
            <option value="Party">Party</option>
        </select>
        <button type="button"  className="btn btn-warning btn-lg"><Link to='/home' onClick = { onclick }>Tìm kiếm</Link></button>
    </div>
)
};

export default Search;