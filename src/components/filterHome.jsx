import React, {useState, useEffect} from 'react';
import {DatePicker, Select, Slider, InputNumber, Button} from 'antd';
import ApiRestaurant from '../services/restaurant-service'
import moment from 'moment'
import './filterHome.scss'
 
const propertiesMapping = {
    "bantron": "Bàn tròn",
    "ngoaitroi": "Ngoài trời",
    "sankhau": "Sân khấu",
    "bancong": "Ban công",
    "tiecdung": "Tiệc đứng"
}
const FilterHome = ({setFilterRestaurant, filterRestaurant}) => {
    const [service, setServices] = useState([])
    const [utilities, setUtilities] = useState([])
    const [price, setPrice] = useState([0, 100])
    const [filterValue, setFilterValue] = useState(filterRestaurant)
    const onHandleChangeTypeAddr = (e) => {
        let value = e.target.value
        setFilterRestaurant({...filterRestaurant, typeAddr: value})
        setFilterValue({...filterValue, typeAddr: value})
    }
    const onHandleChangeCapacity = (value) => {
        setFilterRestaurant({...filterRestaurant, capacity: value})
        setFilterValue({...filterValue, capacity: value})
    }
    const onHandleChangeSearch = (e) => {
        let value = e.target.value
        setFilterRestaurant({...filterRestaurant, searchKey: value})
        setFilterValue({...filterValue, searchKey: value})
    }
    const onChangeDate = (value) => {
        console.log(value.format("YYYY-MM-DD"))
        console.log(moment())
    }
    const onChangeTime = (value) => {
        console.log(value)
    }
    const onChangePrice = (value) => { 
        setPrice(value)
        let range = `[${value[0] * 500000}, ${value[1] * 500000}]`
        setFilterRestaurant({...filterRestaurant, rangePrice: range})
        setFilterValue({...filterValue, rangePrice: range})
    }
    const formatter = (value) => {
        return `${value * 500000} `;
    }

    const onChangeUtility = (value) => {
        
        let p = value.join(",")
        setFilterRestaurant({...filterRestaurant, properties: p})
        setFilterValue({...filterValue, properties: p})
    }
    const initListService = async () => {
        const res = await ApiRestaurant.getService()
        if(res.ok) {
            setServices(res.data)
        }
    }
    const initListProperties = async () => {
        const res = await ApiRestaurant.getProperties()
        if(res.ok) {
            setUtilities(res.data)
        }
    }
    const onClickReset = () => {
        setFilterRestaurant({})
        setFilterValue({})
        setPrice([0, 100])
        document.getElementById("formGroupExampleInput").value = null
    }
    useEffect(() => {
        initListService()
        initListProperties()
    }, [])
    return (
        <div className="filter-home col-md-3 border-top border-right">
            <div className="form-group">
                <h3>Tìm tất cả địa điểm, dịch vụ</h3>
                <input type="text" value={filterValue.searchKey} className="form-control" id="formGroupExampleInput" placeholder="Tên nhà hàng" onChange = { onHandleChangeSearch }/>
            </div>
            <h3>Lọc theo loại địa điểm</h3>
            <div className="form-check">
                <input className="form-check-input" type="radio" value="0" checked={!filterValue.typeAddr} name="type-addr" id="addr-all" onChange = { onHandleChangeTypeAddr } />
                <label className="form-check-label">
                Tất cả
                </label><br/>
                
                <input className="form-check-input" type="radio" value="sinhnhat" name="type-addr" id="addr-center" onChange = { onHandleChangeTypeAddr }/>
                <label className="form-check-label">
                Sinh nhật
                </label><br/>
                <input className="form-check-input" type="radio" value="hoinghi" name="type-addr" id="addr-restaurant" onChange = { onHandleChangeTypeAddr }/>
                <label className="form-check-label">
                Hội thảo/Hội nghị
                </label><br/>
                <input className="form-check-input" type="radio" value="lienhoan" name="type-addr" id="addr-restaurant" onChange = { onHandleChangeTypeAddr }/>
                <label className="form-check-label">
                Liên hoan/Tiệc
                </label><br/>
                <input className="form-check-input" type="radio" value="hopnhom" name="type-addr" id="addr-restaurant" onChange = { onHandleChangeTypeAddr }/>
                <label className="form-check-label">
                Họp nhóm
                </label><br/>
                <input className="form-check-input" type="radio" value="workshop" name="type-addr" id="addr-restaurant" onChange = { onHandleChangeTypeAddr }/>
                <label className="form-check-label">
                Workshop
                </label><br/>
                <input className="form-check-input" type="radio" value="tieccuoi" name="type-addr" id="addr-hotel" onChange = { onHandleChangeTypeAddr } />
                <label className="form-check-label">
                Tiệc cưới
                </label>
            </div>
            <h3>Lọc theo giá mỗi menu</h3>
            <div className="d-flex justify-content-between">
                <p>{(price[0] * 500000).toLocaleString("it-IT")}</p>
                <p>-</p>
                <p>{(price[1] * 500000).toLocaleString("it-IT", {style : "currency", currency : "VND"})}</p>
            </div>
            <Slider range value={price} tipFormatter={formatter} onChange={onChangePrice}/>
            <div className="form-group capacity">
                <h3>Số lượng khách</h3>
                <InputNumber value={filterValue.capacity} style={{ width: '100%'}} onChange={onHandleChangeCapacity}></InputNumber>
            </div>
            {/* <div className="form-group">
                <h3>Thời gian tổ chức</h3>
                <div className="d-flex">
                    <DatePicker 
                        size="default" 
                        onChange={onChangeDate}    
                    />
                    <Select defaultValue="1" onChange={onChangeTime}>
                        <Option value="1">Sáng</Option>
                        <Option value="2">Tối</Option>
                    </Select>
                </div>
            </div> */}
            <div className="utilities">
                <h3>Tiện ích</h3>
                <Select value={filterValue.properties && filterValue.properties.split(",")} mode="tags" style={{ width: '100%' }} tokenSeparators={[',']} onChange={onChangeUtility}>
                    {utilities.map((e, idx) => {
                        return <Option value={e} key={idx}>{propertiesMapping[e]}</Option>
                    })}
                </Select>
            </div>
            <div className="reset" style={{ marginTop: '10px'}}>
                <Button type="primary" onClick={onClickReset}>Reset</Button> 
            </div>
        </div>
    )};
export default FilterHome;
