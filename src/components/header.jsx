import React, {useEffect, useState, useContext} from 'react';
import Search from './search';
import logo from '../images/logo.png';
import {
  Link,
  useHistory
} from "react-router-dom";
import { ShoppingCartOutlined, LogoutOutlined, FileDoneOutlined, HomeOutlined, UserOutlined, SearchOutlined, ReconciliationOutlined } from "@ant-design/icons";
import {Popover, message, Select, Modal, InputNumber, Button, Checkbox} from 'antd'
import { isLogin } from '../plugins/localstorage'
import './header.scss'
import ModalPopup from '../components/Modal'
import UserContext from '../context/info-context'
import Autocomplete from "./Autocomplete";
import ApiRestaurant from '../services/restaurant-service';
import QuoteContext from '../context/quote-context'
const { Option } = Select
const GOOGLE_MAPS_API_KEY = process.env.REACT_APP_GG_MAP_API;
const loadGoogleMapScript = (callback) => {
    if (
      typeof window.google === "object" &&
      typeof window.google.maps === "object"
    ) {
      callback();
    } else {
      const googleMapScript = document.createElement("script");
      googleMapScript.src = `https://maps.googleapis.com/maps/api/js?key=${GOOGLE_MAPS_API_KEY}&libraries=places`;
      window.document.body.appendChild(googleMapScript);
      googleMapScript.addEventListener("load", callback);
    }
};
const listEvent = [
    { value: 'tieccuoi', label: 'Tiệc cưới'},
    { value: 'hoinghi', label: 'Hội nghị/hội thảo'},
    { value: 'lienhoan', label: 'Liên hoan/Tiệc'},
    { value: 'sinhnhat', label: 'Sinh nhật'},
    { label: 'Họp nhóm', value: "hopnhom" },
    { label: 'Workshop', value: "workshop" },
]
const propertiesMapping = {
    "bantron": "Bàn tròn",
    "ngoaitroi": "Ngoài trời",
    "sankhau": "Sân khấu",
    "bancong": "Ban công",
    "tiecdung": "Tiệc đứng"
}
const Header = (props) => {
    const history = useHistory()
    const userContext = useContext(UserContext)
    const { userInfo } = useContext(UserContext)
    const {quoteValue, setQuoteValue} = useContext(UserContext)
    const role = userContext.userInfo.role
    const [typeBtn, setTypeBtn] = useState()
    const changeSearch = () => {
        
    }
    const onClickCart = () => {
        history.push("/cart")
    }
    const onClickLogout = () => {
        localStorage.removeItem('user_info')
        userContext.getUserInfo()
        history.push("/home")
        message.success('Đăng xuất thành công')
    }
    const onClickRequest = () => {
        history.push("/request")
    }
    const onClickMyRes = () => {
        history.push(`/mypage`)
    }
    const onClickStatis = () => {
        history.push(`/statis`)
    }
    const onClickUserInfo = () => {
        history.push(`/userinfo`)
    }
    const onClickListUser = () => {
        history.push(`/listuser`)
    }
    const onClickUserOrder = () => {
        history.push(`/users/orders`)
    }
    const onClickAdminOrder = () => {
        history.push(`/admin/orders`)
    }
    const content = () => {
        return <div className="avatar-dropdown">
            <div className="avatar-dropdown__item logout" onClick={onClickLogout}>
                <span className="icon"><LogoutOutlined /></span>
                <span className="text">Đăng xuất</span>
            </div>
            {["sys_admin", "rest_admin", "user"].includes(role) && <div className="avatar-dropdown__item info" onClick={onClickUserInfo}>
                <span className="icon"><UserOutlined /></span>
                <span className="text">Thông tin người dùng</span>
            </div>}
            {["sys_admin"].includes(role) && <div className="avatar-dropdown__item info" onClick={onClickListUser}>
                <span className="icon"><UserOutlined /></span>
                <span className="text">Quản lý người dùng</span>
            </div>}
            {["user", "sys_admin", "rest_admin"].includes(role) && <div className="avatar-dropdown__item info" onClick={onClickUserOrder}>
                <span className="icon"><ShoppingCartOutlined /></span>
                <span className="text">Đơn hàng đã đặt</span>
            </div>}
            {["rest_admin"].includes(role) && <div className="avatar-dropdown__item info" onClick={onClickAdminOrder}>
                <span className="icon"><ShoppingCartOutlined /></span>
                <span className="text">Quản lý đơn hàng</span>
            </div>}
            {role === "sys_admin" && <div className="avatar-dropdown__item request" onClick={onClickRequest}>
                <span className="icon"><FileDoneOutlined /></span>
                <span className="text">Danh sách yêu cầu</span>
            </div>}
            {role === "rest_admin" && <div className="avatar-dropdown__item myrest" onClick={onClickMyRes}>
                <span className="icon"><HomeOutlined /></span>
                <span className="text">Nhà hàng của tôi</span>
            </div>}
            {/* <div className="avatar-dropdown__item myrest" onClick={onClickStatis}>
                <span className="icon"><LineChartOutlined /></span>
                <span className="text">Thống kê</span>
            </div> */}
        </div>
    }
    useEffect(() => {
        const pathname = history.location.pathname
    },[])
    const AutoQuote = () => {
        const [event, setEvent] = useState(null)
        const [numberPeople, setNumberPeople] = useState(1)
        const [totalPrice, setTotalPrice] = useState('')
        const [addrDetail, setAddrDetail] = useState('')
        const [coordinate, setCoordinates] = useState(null)
        const [loadMap, setLoadMap] = useState(false);
        const [utilities, setUtilities] = useState([]);
        const [services, setServices] = useState([]);
        const [selectedUtilities, setSelectedUtilities] = useState([]);
        const [selectedServices, setSelectedServices] = useState([]);
        const [tabQuote, setTabQuote] = useState('price');
        const [addressText, setAddressText] = useState('')
        const onChangeEvent = (e) => {
            setEvent(e)
        }
        const onChangePeople = (e) => {
            setNumberPeople(e)
        }
        const onChangePrice = (e) => {
            setTotalPrice(e)
        }
        const onChangeTab = (e) => {
            setTabQuote(e)
        }
        const onClickSearch = () => {
            if(!event) return message.error("Vui lòng chọn loại hình")
            if(!numberPeople) return message.error("Vui lòng nhập số người")
            // if(tabQuote === 'price' && !totalPrice) return message.error("Vui lòng nhập chi phí")
            // if(tabQuote !== 'price' && !coordinate) return message.error("Vui lòng nhập địa chỉ")
            let dataQuote = { 
                type: event, quantity: numberPeople, 
                price: totalPrice, coordinate, 
                properties: selectedUtilities, services: selectedServices ,
                priority: tabQuote,
                addressText
            }
            setQuoteValue(dataQuote)
            history.push(`/quote`)
            Modal.destroyAll()
        }
        useEffect(() => {
            loadGoogleMapScript(() => {
              setLoadMap(true);
            });
          }, []);

        const handleChangeUtilites = (value) => {
            setSelectedUtilities(value);
        }
        const handleChangeServices = (value) => {
            setSelectedServices(value);
        }
        const initListService = async () => {
            const res = await ApiRestaurant.getService()
            if(res.ok) {
                setServices(res.data)
            }
        }
        const initListProperties = async () => {
            const res = await ApiRestaurant.getProperties()
            if(res.ok) {
                setUtilities(res.data)
            }
        }
        const onChangePriority = (v) => {
            setTabQuote(v)
        }
        useEffect(() => {
            initListService()
            initListProperties()
        }, [])
        return <div className="auto-quote">
            {/* <div className="block-1">
                <div className={`block-1__item${tabQuote === 'price' ? '-active' : ''}`} onClick={() => onChangeTab('price')}>
                    Theo chi phí
                </div>
                <div className={`block-1__item${tabQuote === 'address' ? '-active' : ''}`} onClick={() => onChangeTab('address')}>
                    Theo vị trí
                </div>
            </div> */}
            <div className="block-2">
                <div className="block-2__item totalPrice">
                    <h3>* Loại hình sự kiện</h3>
                    <Select style={{ width: '100%' }} value={event} onChange={onChangeEvent}>
                        {listEvent.map(event => (
                            <Select.Option value={event.value} key={event.value}>{event.label}</Select.Option>
                        ))}
                    </Select>
                </div>
                <div className="block-2__item capacity">
                    <h3>* Số người</h3>
                    <InputNumber value={numberPeople} onChange={onChangePeople}></InputNumber>
                </div>
            </div>
            <div className="block-4 block-45">
                <div className="block-4__item price block-45__item">
                    <div className="item-top">
                        <h3>Chi phí dự tính</h3>
                        {/* <div className="right">
                            Ưu tiên <Checkbox onChange={() => onChangePriority("price")} checked={tabQuote === "price"}></Checkbox>
                        </div> */}
                    </div>
                    <div style={{ display: 'flex', alignItems: 'center'}}>
                    <InputNumber
                        value={totalPrice}
                        onChange={onChangePrice}
                        style={{width: "100%"}}
                        formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                        parser={value => value.replace(/\$\s?|(,*)/g, '')}
                    ></InputNumber>VND
                    </div>
                </div>
            </div>
            <div className="block-5 block-45">
                <div className="block-5__item address block-45__item">
                    <div className="item-top">
                        <h3>Địa chỉ gần nhất</h3>
                        {/* <div className="right">
                            Ưu tiên <Checkbox onChange={() => onChangePriority("address")} checked={tabQuote === "address"}></Checkbox>
                        </div> */}
                    </div>
                    <Autocomplete 
                        setAddrDetail = {setAddrDetail}
                        setCoordinates = {setCoordinates}
                        addressText = {addressText}
                        setAddressText = {setAddressText}
                        name="address_detail"
                    />
                </div>
            </div>
            {/* <div className="block11">
                <h3>Ưu tiên lọc theo: </h3>
                <Select>
                    <Option>Chi phí</Option>
                    <Option>Địa chỉ</Option>
                </Select>
            </div> */}
            <div className="block-3">
                <div className="block-3__item utilities">
                    <h3>Tiện ích</h3>
                    <Select mode="tags" style={{ width: '100%' }} onChange={handleChangeUtilites} tokenSeparators={[',']}>
                        {utilities.map((e, idx) => {
                            return <Option key={idx} value={e}>{propertiesMapping[e]}</Option>
                        })}
                    </Select>
                </div>
            </div>
            <div className="block-3">
                <div className="block-3__item services">
                    <h3>Dịch vụ</h3>
                    <Select mode="tags" style={{ width: '100%' }} onChange={handleChangeServices} tokenSeparators={[',']}>
                        {services.map((e, idx) => {
                            return <Option key={idx} value={e.type}>{e.name}</Option>
                        })}
                    </Select>
                </div>
            </div>
            <div className="btn-group" style={{ marginTop: '20px', display: 'flex', justifyContent: 'flex-end'}}>
                <Button onClick={() => Modal.destroyAll()}>Cancel</Button>
                <Button type="primary" style={{ marginLeft: '10px'}} onClick={onClickSearch}>Tìm kiếm</Button>
            </div>
        </div>
    }
    const modalAuto = () => {
        return  ModalPopup({
            // type: "confirm",
            title: "Báo giá tự động",
            okText: "Tìm kiếm",
            content: () => <AutoQuote />,
            propsContent: {}
        })
    }
    return (
        <div className="wrap-header">
            <div className="header">
                <div className="header-left">
                    <Link to="/"><img src={logo} alt="logo"></img></Link>

                </div>
                <div className="header-center">
                    {/* {["/home", "/"].includes(history.location.pathname)  ? */}
                        <Button size="large" className="btn btn-lg"  onClick={modalAuto} className="btn-quote">
                            Báo giá tự động cho sự kiện của bạn
                            <SearchOutlined style={{ color: '#fcae02'}}/>
                        </Button> 
                            {/* :
                        <Button type="primary" size="large" className="btn btn-lg"  onClick={()=> history.push('/home')}>
                            
                            Danh sách nhà hàng
                        </Button>
                    } */}
                </div>
                <div className="header-right">
                    <div className="cart" onClick={onClickCart}>
                        <ShoppingCartOutlined 
                            style={{ fontSize: '30px', color: 'white', cursor: 'pointer' }}
                        />

                    </div>
                    <div className="signup-restaurant">
                        {role === "user" && 
                        <Button className="btn-cooporate" size="large" onClick={()=> history.push('/signuprestaurant')}>
                            <div className="icon"><ReconciliationOutlined className="icon1" /></div>
                            Trở thành đối tác
                        </Button>
                        }
                    </div>
                    <div className="signup-user">
                        {/* <div className="name">
                            <span>Hi,</span>
                            <span>{userInfo.info && userInfo.info.name.split(" ")[0]}</span>
                        </div> */}
                        {!isLogin() ? <Button type="primary" size="large" className="btn btn-lg"><Link to="/signupuser">Đăng nhập</Link></Button> : 
                            <Popover placement="bottomRight" title="Cài đặt" content={content} trigger="click">
                                <div className="avatar">
                                    <img src="https://static2.yan.vn/YanNews/2167221/202102/facebook-cap-nhat-avatar-doi-voi-tai-khoan-khong-su-dung-anh-dai-dien-e4abd14d.jpg" alt=""/>
                                </div>
                            </Popover>
                        }
                    </div>
                </div>
            </div>
        </div>
    ) 
};
export default Header;
