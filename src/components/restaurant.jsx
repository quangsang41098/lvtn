import React from 'react';
import {Link} from 'react-router-dom'
import { useHistory } from "react-router-dom";
import  './restaurant.scss';
import { Card } from 'antd';
import { EnvironmentOutlined, HomeOutlined } from "@ant-design/icons";
import imgdetail1 from '../images/detail1.jpg'
const styleImg = {
    width: '100%',
    height: '150px'
}

const Restaurant = (props) => {
    const { info } = props
    const history = useHistory()
    const onClickRestaurant = id => {
        window.scrollTo(0, 0);
        history.push(`/page-detail/${id}`)
    }
    return (
        <div className="card w-33 mb-20" onClick={() => onClickRestaurant(info._id)}>
            <img className="card-img-top" src={info.listImage[0] || imgdetail1} alt="Card cap" style = {styleImg }/>
            <div className="card-body">
                <h5 className="card-title">{info.name}</h5>
                <p className="card-text address"><EnvironmentOutlined  style={{ color: "#002f79"}}/> 
                    {` ${info.address}`}
                </p>
                <div className="venue"><HomeOutlined  style={{ color: "#002f79", marginRight: '5px'}}/>{info.totalVenues} phòng sảnh</div>
                <p className="card-text" style={{ color: "#002f79", margin: "0"}}>Khoảng giá</p>
                <p className="card-text" style={{ color: "#fcae02"}}>
                    {info.rangeMenuPrice[0].toLocaleString("it-IT")} 
                    - {info.rangeMenuPrice[1].toLocaleString("it-IT")} VND
                </p>
            </div>
        </div>  
    ) 
};
export default Restaurant;
