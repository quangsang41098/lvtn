import React, { useEffect, useRef, useState } from "react";
import {Input } from 'antd'
const Autocomplete = (props) => {
  const {setAddrDetail, setCoordinates, setPlaceId, addressText, setAddressText} = props
  const placeInputRef = useRef(null);
  const [place, setPlace] = useState(null);

  useEffect(() => {
    initPlaceAPI();
    if(addressText) document.getElementById("autocomplete").value = addressText;
  }, []);

  // initialize the google place autocomplete
  const initPlaceAPI = () => {
    let autocomplete = new window.google.maps.places.Autocomplete(
      placeInputRef.current
    );

    // Set autocomplete fields
    autocomplete.setFields(["formatted_address", "geometry", "place_id"]);

    new window.google.maps.event.addListener(
      autocomplete,
      "place_changed",
      function () {
        let place = autocomplete.getPlace();
        console.log(place)
        if(setAddressText) setAddressText(place.formatted_address)
        if(setPlaceId) setPlaceId(place.place_id)
        setAddrDetail(place.formatted_address)
        setCoordinates([
          place.geometry.location.lat(),
          place.geometry.location.lng()
        ])
        // console.log(`[${place.geometry.location.lat()},${place.geometry.location.lng()}]`)
      }
      
    );
  };

  return (
    <>
      <input
        type="text"
        id="autocomplete"
        ref={placeInputRef}
        style={{
          // fontSize: "1.5em",
          color: "rgba(0, 0, 0, 0.85)",
          border: "1px solid #d9d9d9",
          width: "100%",
          padding: "4px 11px",
          outline: "none",
        }}
      />
      
      {/* <Input ref={placeInputRef}  placeholder="Địa chỉ"/> */}
      {/* {place && (
        <div style={{ marginTop: 20, lineHeight: "25px" }}>
          <h3>Selected Place</h3>
          <div>
            <b>Address:</b> {place.address}
          </div>
          <div>
            <b>Lat:</b> {place.lat}
          </div>
          <div>
            <b>Lng:</b> {place.lng}
          </div>
        </div>
      )} */}
    </>
  );
};

export default Autocomplete;
