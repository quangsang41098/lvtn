import React from 'react';
import './Modal.scss'
import { Modal } from 'antd'

const { info, confirm } = Modal
export default ({
    type,
    text,
    className,
    onOk,
    title,
    okText,
    content,
    propsContent
}) => {
  const Content = content
    switch (type) {
        case "error":
          confirm({
              className: className,
              title: <h3>{title}</h3> || "Something went wrong",
              okText: okText || 'OK',
              icon: null,
              centered: true,
              maskClosable: true,
            });
            break;
        case "confirm":
          confirm({
              className: className,
              title: <h3>{title}</h3> || "",
              okText: okText || 'OK',
              icon: null,
              centered: true,
              maskClosable: true,
              onOk: typeof onOk === 'function' ? onOk : null,
              content: content ? <Content {...propsContent} /> : '',
            });
            break;
    
        default:
            confirm({
                title: <h3>{title}</h3> || "",
                className: `empty-modal${className ? ` ${className}`:''}`,
                centered: true,
                content: content ? <Content {...propsContent} /> : '',
                icon: null,
                maskClosable: true,
            })
            break;
    }
};
// export default ModalPopup;
