import React, { useEffect, useState } from "react";
import { StripeProvider } from "react-stripe-elements";
import {
	BrowserRouter as Router,
	Switch,
	Route,
	// Link,
	// useRouteMatch
} from "react-router-dom";
import PaymentPage from "./pages/payment-page/PaymentPage";
import SearchPage from './pages/SearchPage';
import HomePage from './pages/HomePage';
import SignUpRestaurantPage from './pages/SignUpRestaurantPage'
import PageDetail from './pages/page-detail/PageDetail'
import MyPage from './pages/page-detail/MyPage'
import CartPage from './pages/cart-page/CartPage'
import Login from './pages/login/Login'
import AutoQuote from './pages/auto-quote/AutoQuote'
import Header from './components/header'
import RequestPage from './pages/request-page/RequestPage'
import UserInfo from './pages/user-info/UserInfo'
import Statistic from './pages/statistic/Statistic'
import Verify from './pages/verify/Verify'
import UserOrder from './pages/order/Order'
import AdminOrder from './pages/order/AdminOrder'
import Footer from './components/footer'
import './App.scss';
import axios from 'axios'
import 'antd/dist/antd.css';
import UserContext from './context/info-context'
import ApiUser from './services/user-service'
import { isLogin } from './plugins/localstorage'
import ListUser from "./pages/list-user/ListUser";
import { BackTop } from 'antd'
import { UpOutlined } from '@ant-design/icons'
const style = {
  height: 40,
  width: 40,
  lineHeight: '40px',
  borderRadius: 999,
  backgroundColor: '#EEE',
  color: '#fff',
  textAlign: 'center',
  fontSize: 14,
};

const App = () => {
  const [quoteValue, setQuoteValue] = useState({
    type:null,
    quantity: 1,
    price: 0,
    coordinate: null,
    properties: [],
    services: []
  })
  const [userInfo, setUserInfo] = useState({
    info: null,
    role: "guest"
  })
  const [selectedCart, setSelectedCart] = useState(null)
  const DashboardLayoutRoute = ({ component: Component, ...rest }) => {
    return (
      <Route
        {...rest}
        render={(props) => {
          return (
            
            <div className='wrap-app'>
              <Header />
              <div className="wrap-dashboard">
                <Component />
              </div>
              <BackTop>
                <div className="backtop"><UpOutlined /></div>
              </BackTop>
              {/* <Footer /> */}
            </div>
          )
        }}
      >
      </Route>
    )
  }
  const getUserInfo = async () => {
    if(!isLogin()) return setUserInfo({
      info: null,
      role: 'guest'
    })
    const res = await ApiUser.getUserInfo()
    let role = 'user'
    let userInfo = res.data
    if(userInfo.isSystemAdmin) role = "sys_admin"
    if(userInfo.restAdmin.length > 0) role = "rest_admin"
    setUserInfo({
      info: userInfo,
      role
    })
  }
  useEffect(() => {
    getUserInfo()
  },[])
  return (
    <div className="App">
      <UserContext.Provider value={{ userInfo, getUserInfo, quoteValue, setQuoteValue, selectedCart, setSelectedCart}}>
      <Router>
          <Switch>
            <Route exact path="/signupuser">
              <Login />
            </Route>
            <Route exact path="/verify-account">
              <Verify />
            </Route>
            <DashboardLayoutRoute
              exact
              path="/"
              component={HomePage}
            ></DashboardLayoutRoute>
            <DashboardLayoutRoute
              exact
              path="/home"
              component={HomePage}
            ></DashboardLayoutRoute>
            <DashboardLayoutRoute
              exact
              path="/quote"
              component={AutoQuote}
            ></DashboardLayoutRoute>
            
            <DashboardLayoutRoute
              exact
              path="/signuprestaurant"
              component={SignUpRestaurantPage}
            ></DashboardLayoutRoute>
            <DashboardLayoutRoute
              exact
              path="/page-detail/:id"
              component={PageDetail}
            ></DashboardLayoutRoute>
            <DashboardLayoutRoute
              exact
              path="/mypage"
              component={MyPage}
            ></DashboardLayoutRoute>
            <DashboardLayoutRoute
              exact
              path="/cart"
              component={CartPage}
            ></DashboardLayoutRoute>
            <DashboardLayoutRoute
              exact
              path="/request"
              component={RequestPage}
            ></DashboardLayoutRoute>
						<DashboardLayoutRoute
              exact
              path="/payment"
              component={PaymentPage}
            ></DashboardLayoutRoute>
						<DashboardLayoutRoute
              exact
              path="/userinfo"
              component={UserInfo}
            ></DashboardLayoutRoute>
						<DashboardLayoutRoute
              exact
              path="/statis"
              component={Statistic}
            ></DashboardLayoutRoute>
						<DashboardLayoutRoute
              exact
              path="/listuser"
              component={ListUser}
            ></DashboardLayoutRoute>
						<DashboardLayoutRoute
              exact
              path="/users/orders"
              component={UserOrder}
            ></DashboardLayoutRoute>
						<DashboardLayoutRoute
              exact
              path="/admin/orders"
              component={AdminOrder}
            ></DashboardLayoutRoute>
          </Switch>
        </Router>
        </UserContext.Provider>
    </div>
  )
}

export default App;
