import { findIndex } from 'lodash'

export const loadStateFromLocal = () => {
	try {
		const serializedState = localStorage.getItem("cart");
		if (serializedState === null) {
			return [];
		}
		return JSON.parse(serializedState);
	} catch (err) {
		return [];
	}
};

export const saveStateToLocal = (state) => {
	try {
		const serializedState = JSON.stringify(state);
		localStorage.setItem("cart", serializedState);
	} catch (err) {}
};

export const addCartToLocal = (state) => {
	console.log(state)
	try {
		let cart = JSON.parse(localStorage.getItem("cart"));
		if(!cart) {
			const serializedState = JSON.stringify([{
				restaurant_name: state.restaurant_name,
				restaurant_id: state.restaurant_id,
				venueId: state.venueId,
				selectedDate: state.selectedDate,
				list_item: [state.item]
			}]);
			localStorage.setItem("cart", serializedState);
		} else {
			let res = findIndex(cart, { restaurant_id: state.restaurant_id });
			if(res === -1) {
				cart.push({
					restaurant_name: state.restaurant_name,
					restaurant_id: state.restaurant_id,
					venueId: state.venueId,
					selectedDate: state.selectedDate,
					list_item: [state.item]
				});
				localStorage.setItem("cart", JSON.stringify(cart));
			} else {
				if(state.item_type === "service") {
					let indexItem = findIndex(cart[res].list_item, { item_id: state.item.item_id });
					if(indexItem === -1) {
						cart[res].list_item.push(state.item);
					}
				}
				else {
					let indexItem = findIndex(cart[res].list_item, { item_id: state.item.item_id });
					if(!cart[res].venueId) cart[res].venueId = state.venueId
					if(cart[res].venueId !== state.venueId) {
						cart[res].venueId = state.venueId
						cart[res].list_item = [state.item]
					}
					else if(indexItem === -1) {
						cart[res].list_item.push(state.item);
					} else {
						cart[res].list_item[indexItem].item_quantity += state.item.item_quantity;
					}

				}
			}
			localStorage.setItem("cart", JSON.stringify(cart));
		}
	} catch (err) {}
};


export const editCartItemToLocal = (state) => {
	try {
		let cart = JSON.parse(localStorage.getItem("cart"));
		if(cart) {
			let res = findIndex(cart, { restaurant_id: state.restaurant_id });
			if(res !== -1) {
				let indexItem = findIndex(cart[res].list_item, { item_id: state.item.item_id });
				if(indexItem !== -1) {
					cart[res].list_item[indexItem].item_quantity = state.item.item_quantity;
					localStorage.setItem("cart", JSON.stringify(cart));
					return cart;
				}
			}
		}
	} catch (err) {}
};

export const removeCartItemToLocal = (state) => {
	try {
		let cart = JSON.parse(localStorage.getItem("cart"));
		if(cart) {
			let res = findIndex(cart, { restaurant_id: state.restaurant_id });
			if(res !== -1) {
				let indexItem = findIndex(cart[res].list_item, { item_id: state.item.item_id });
				if(indexItem !== -1) {
					cart[res].list_item.splice(indexItem, 1);
					if(cart[res].list_item.length === 0) cart.splice(res, 1);
					localStorage.setItem("cart", JSON.stringify(cart));
					return cart;
				}
			}
		}
	} catch (err) {}
};

export const isLogin = () => {
  try {
    let user_info = JSON.parse(localStorage.getItem('user_info'));
    return user_info && user_info.auth
  } catch (err) {}
};
